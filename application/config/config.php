<?php


error_reporting(E_ALL);
ini_set("display_errors", 1);


ini_set('session.cookie_httponly', 1);


return array(

    'URL' => 'http://' . $_SERVER['HTTP_HOST'] . str_replace('public', '', dirname($_SERVER['SCRIPT_NAME'])),
    'BASE_URL' => 'http://ajtest.local',

    'PATH_CONTROLLER' => realpath(dirname(__FILE__).'/../../') . '/application/controller/',
    'PATH_VIEW' => realpath(dirname(__FILE__).'/../../') . '/application/view/',

    'PATH_AVATARS' => realpath(dirname(__FILE__).'/../../') . '/public/avatars/',
    'PATH_AVATARS_PUBLIC' => 'avatars/',

    'DEFAULT_CONTROLLER' => 'index',
    'DEFAULT_ACTION' => 'index',

    'DB_TYPE' => 'mysql',
    'DB_HOST' => '127.0.0.1',
    'DB_NAME' => 'corephp',
    'DB_USER' => 'admin',
    'DB_PASS' => 'home',
    'DB_PORT' => '3306',
    'DB_CHARSET' => 'utf8',

    // 'DB_TYPE' => 'mysql',
    // 'DB_HOST' => '127.0.0.1',
    // 'DB_NAME' => 'corephp',
    // 'DB_USER' => 'corephp',
    // 'DB_PASS' => 'Ajay@123',
    // 'DB_PORT' => '3306',
    // 'DB_CHARSET' => 'utf8',

    'CAPTCHA_WIDTH' => 359,
    'CAPTCHA_HEIGHT' => 100,

    'COOKIE_RUNTIME' => 1209600,
    'COOKIE_PATH' => '/',
    'COOKIE_DOMAIN' => "",
    'COOKIE_SECURE' => false,
    'COOKIE_HTTP' => true,
    'SESSION_RUNTIME' => 604800,

    'USE_GRAVATAR' => false,
    'GRAVATAR_DEFAULT_IMAGESET' => 'mm',
    'GRAVATAR_RATING' => 'pg',
    'AVATAR_SIZE' => 44,
    'AVATAR_JPEG_QUALITY' => 85,
    'AVATAR_DEFAULT_IMAGE' => 'default.jpg',

    'ENCRYPTION_KEY' => '6#x0gÊìf^25cL1f$08&',
    'HMAC_SALT' => '8qk9c^4L6d#15tM8z7n0%',

    'EMAIL_USED_MAILER' => 'phpmailer',
    'EMAIL_USE_SMTP' => true,
    'EMAIL_SMTP_HOST' => 'smtp.gmail.com',
    'EMAIL_SMTP_AUTH' => true,
    'EMAIL_SMTP_USERNAME' => 'ajayit2020@gmail.com',
    'EMAIL_SMTP_PASSWORD' => 'Ajay@9711',
    'EMAIL_SMTP_PORT' => 587,
    'EMAIL_SMTP_ENCRYPTION' => 'tls',
  
    'EMAIL_PASSWORD_RESET_URL' => 'login/verifypasswordreset',
    'EMAIL_PASSWORD_RESET_FROM_EMAIL' => 'no-reply@example.com',
    'NO_REPLAY' => 'no-reply@example.com',
    'PROFILE_UPDATE' => 'Your profile is updated',
    'PROFILE_UPDATE_SUBJECT' => 'Profile Updation',
    'EMAIL_PASSWORD_RESET_FROM_NAME' => 'Helyos',
    'EMAIL_PASSWORD_RESET_SUBJECT' => 'Password reset for PROJECT XY',
    'EMAIL_PASSWORD_RESET_CONTENT' => 'Please click on this link to reset your password: ',
    'EMAIL_VERIFICATION_URL' => 'register/verify',
    'EMAIL_VERIFICATION_FROM_EMAIL' => 'no-reply@example.com',
    'EMAIL_VERIFICATION_FROM_NAME' => 'My Project',
    'EMAIL_VERIFICATION_SUBJECT' => 'Account activation for PROJECT XY',
    'EMAIL_VERIFICATION_CONTENT' => 'Please click on this link to activate your account: ',
);
