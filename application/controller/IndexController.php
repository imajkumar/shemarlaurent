<?php

class IndexController extends Controller
{
    /**
     * Construct this object by extending the basic Controller class
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Handles what happens when user moves to URL/index/index - or - as this is the default controller, also
     * when user moves to /index or enter your application at base level
     */
    public function paynow(){
        $data=UserModel::paynow();
       

    }

     public function seller_code(){
        UserModel::getSellerAuthcode();
     }
    public function index()
    {

        $data=[
            "products"=>ProductModel::getAllProuducts()
        ];
        $this->View->renderFront('index/index',$data);



    }
    public function myaccount()
    {      

       


        $user_id=Session::get('user_id');
       if (isset($user_id)) {
            $this->View->renderFront('index/showProfile', array(
                'user' => UserModel::getPublicProfileOfUser($user_id))
            );
        } else {
            Redirect::home();
        }


    }
   
}
