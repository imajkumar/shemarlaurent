<?php


class ProductController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    
    public function checkout()
    {          
        Auth::checkAuthentication();

        $data=[
            "products_details"=>''
        ];
        $this->View->renderFront('index/check_out',$data);



       
    }

    public function view($product_id)
    {

          
        $data=[
            "products_details"=>ProductModel::getProductWithID($product_id)
        ];
        $this->View->renderFront('index/single_product',$data);

       
    }
    public function edit($product_id)
    {
       

          
        $data=[
            "products"=>ProductModel::getProductWithID($product_id)
        ];
       

        $this->View->renderSeller('seller/products_edit',$data);
       
       
    }

    public function addToCart()
    {
       
        ProductModel::addToCart(
            Request::post('product_id'),
            Request::post('quantity')
           
            
        );


    }
    public function removeFromCart()
    {
       
        ProductModel::removeFromCart(
            Request::post('code')
            
           
            
        );


    }

    

    public function carts()
    {
       
            
        $data=[
            "products_details"=>''
        ];
        $this->View->renderFront('index/product_carts',$data);


    }
    

    

    
}
