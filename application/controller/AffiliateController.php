<?php

class AffiliateController extends Controller
{

    public function __construct()
    {
        parent::__construct();       
        Auth::checkAffiliateAuthentication();
    }


    public function index()
    {
        $this->View->renderAffiliate('affiliate/index', array(
                'user_data' => AdminModel::getAdminData())
        );
    }

    /*Affiliate Setting*/
    public function Settings()
    {
        $this->View->renderAffiliate('affiliate/affiliate-settings', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    public function general()
    {
        $this->View->renderAffiliate('affiliate/general', array(
                'general' => AffiliateModel::getGeneral())
        );
    }
    public function payment()
    {
        $this->View->renderAffiliate('affiliate/payment', array(
                'general' => AffiliateModel::getPayment())
        );
    }
    public function legal()
    {
        $this->View->renderAffiliate('affiliate/legal', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    /*End of Settings*/

    /*Code Affiliate*/
    public function code()
    {
        $this->View->renderAffiliate('affiliate/code', '');
    }
    /*Analytics Affiliate*/
    public function analytics()
    {
        $this->View->renderAffiliate('affiliate/analytics', '');
    }
    public function messengers()
    {
        $this->View->renderAffiliate('affiliate/messangers', ''
        );
    }
    public function myaccount(){
        $this->View->renderAffiliate('affiliate/myaccount', array(
            'user_data' => AdminModel::getAdminData())
        );
    }
    public function sendChat(){
        $chat = chatModel::sendtheChat("seller");
    }
    public function fetchChat(){
        $fetchchatmsg = chatModel::fetchtheChat("seller");
    }
    public function emailupdate(){
        $userId = Session::get('user_id');
        $userEmail = Request::post('id');
        $emailUpdate = UserModel::saveNewEmailAddress($userId, $userEmail);       

    }
    public function affsetting(){
        $normalSettings = AffiliateModel::saveAffSettings();
    }
    public function shipsetting(){
        $shipSettings = AffiliateModel::saveShipSettings();
    }
    public function paymentSetting(){
        $paySettings = AffiliateModel::savePaySettings();
    }
}
