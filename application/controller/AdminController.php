<?php

class AdminController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        // special authentication check for the entire controller: Note the check-ADMIN-authentication!
        // All methods inside this controller are only accessible for admins (= users that have role type 7)
        Auth::checkAdminAuthentication();
    }

    public function seller(){
        $this->View->renderAdmin('admin/seller', array(
            'seller_data' => UserModel::getAllSellerUser())
       );

    }
    public function affiliate(){
        $this->View->renderAdmin('admin/affiliate', array(
            'affiliate_data' => UserModel::getAllAffiliateUser())
       );

    }
    //MyAccount Page
    public function myaccount(){
        $this->View->renderAdmin('admin/myaccount', array(
            'admin_data' => AdminModel::getAdminData())
       );

    }
    //order admin Page
    public function order(){
        $this->View->renderAdmin('admin/order', '' 
       );

    }
    //Admin Setting Page
    public function settings(){
        $this->View->renderAdmin('admin/settings', '' 
       );

    }
    public function general(){
        $this->View->renderAdmin('admin/general', '' 
       );

    }
    public function legal(){
        $this->View->renderAdmin('admin/legal', '' 
       );

    }
    //Admin Messanger Page
    public function messanger(){
        $this->View->renderAdmin('admin/messanger', '' 
       );

    }
    public function sendChat(){
        $chat = ChatModel::sendtheChat("admin");
    }
    public function chatUser(){
        $users = ChatModel::ChatUsers();
    }
    public function fetchChat(){
        $fetchchatmsg = ChatModel::fetchtheChat("admin");
    }
    //Admin Dashboard Page
     public function dashboard(){
        $this->View->renderAdmin('admin/dashboard', '');

    }
    //Vents Page
    public function vents(){
        $this->View->renderAdmin('admin/vents', array(
            'vents_data' => AdminModel::getventData())
       );

    }
    //Plazma Beta
    public function plazmabeta(){
        $this->View->renderAdmin('admin/plazmabeta', array(
            'vents_data' => AdminModel::getventData())
       );

    }
    public function seller_approved(){
        UserModel::seller_approved();
    }
    public function affiliate_approved(){
        UserModel::affiliate_approved();
    }    
    public function seller_code(){
        UserModel::getAuthCode();
    }
    public function seller_code_save(){
      
        UserModel::getAuthCodeSave();
    }
    
    public function index(){
        $this->View->renderAdmin('admin/index', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    public function products(){
        $this->View->renderAdmin('admin/all_products', array(
            'products' => ProductModel::getAllProuducts()) 
        );
    }
    public function updateproduct(){
        ProductModel::updatetheProduct();
    }
    public function actionAccountSettings(){
        AdminModel::setAccountSuspensionAndDeletionStatus(
            Request::post('suspension'), Request::post('softDelete'), Request::post('user_id')
        );

        Redirect::to("admin");
    }
    public function updateProfile(){
        AdminModel::updateAdmin();
    }
}
