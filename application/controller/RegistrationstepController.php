<?php

class RegistrationStepController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        // special authentication check for the entire controller: Note the check-ADMIN-authentication!
        // All methods inside this controller are only accessible for admins (= users that have role type 7)
       // Auth::checkSellerAuthentication();
    }
 
    /**
     * This method controls what happens when you move to /admin or /admin/index in your app.
     */
    public function index()
    {
        $this->View->renderFrontWithoutHeadFoot('register-step-0/index', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    
    
    


    
}
