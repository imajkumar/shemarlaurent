<?php

class SellerApplyController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        // special authentication check for the entire controller: Note the check-ADMIN-authentication!
        // All methods inside this controller are only accessible for admins (= users that have role type 7)
       // Auth::checkSellerAuthentication();
    }
 
    /**
     * This method controls what happens when you move to /admin or /admin/index in your app.
     */
    public function index()
    {        
        $this->View->renderFrontWithoutHeadFoot('sellerapply/index', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }    
    /*Add New Seller Request*/
    public static function addseller()
    {
        $app_affiliate_suc = RegistrationModel::addSellerin();        
    }
    
    /*Add Subscriber*/
    public static function subscribe()
    {
        $subs = RegistrationModel::addsubscribe();        
    }

    
}
