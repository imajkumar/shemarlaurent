<?php

class SellerController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        // special authentication check for the entire controller: Note the check-ADMIN-authentication!
        // All methods inside this controller are only accessible for admins (= users that have role type 7)
        Auth::checkSellerAuthentication();
    }
 
    /**
     * This method controls what happens when you move to /admin or /admin/index in your app.
     */
    public function index()
    {
        $this->View->renderSeller('seller/index', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    
    public function products()
    {
        $this->View->renderSeller('seller/products', array(
                'products' => ProductModel::getAllProduct(11))
        );
    }

    public function addProducts()
    {
        $this->View->renderSeller('seller/add_products', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    /*seller myaccounnt*/
    public function myaccount()
    {
        $this->View->renderSeller('seller/myaccount', array(
                'user_data' => AdminModel::getAdminData())
        );
    }
    /*seller order*/
    public function sellerOrder()
    {
        $this->View->renderSeller('seller/seller-order', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    /*Seller Setting*/
    public function sellerSettings()
    {
        $this->View->renderSeller('seller/seller-settings', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    public function general()
    {
        $this->View->renderSeller('seller/general', array(
                'genInfo' => sellerModel::getGen())
        );
    }
    public function payment()
    {
        $this->View->renderSeller('seller/payment', array(
                'general' => sellerModel::getPayment())
        );
    }
    public function legal()
    {
        $this->View->renderSeller('seller/legal', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    public function shipping()
    {
        $this->View->renderSeller('seller/shipping', array(
                'ships' => sellerModel::getShip())
        );
    }

    /*Seller Analytics*/
    public function selleranalytics()
    {
        $this->View->renderSeller('seller/seller-analytics', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
    public function SaveProducts()
    {
        echo ProductModel::saveProduct();   
    }
    public function updateProducts()
    {
        echo ProductModel::updateProducts();   
    }
    public function updateProductIMG(){
        echo ProductModel::updateProductIMG();   
    }
    

    public function actionAccountSettings()
    {
        AdminModel::setAccountSuspensionAndDeletionStatus(
            Request::post('suspension'), Request::post('softDelete'), Request::post('user_id')
        );

        Redirect::to("admin");
    }
    public function sellersetting(){
        $normalSettings = sellerModel::saveSellerSettings();
    }
    public function shipsetting(){
        $shipSettings = sellerModel::saveShipSettings();
    }
    public function paymentSetting(){
        $paySettings = sellerModel::savePaySettings();
    }
}
