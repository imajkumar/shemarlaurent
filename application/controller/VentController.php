<?php

class VentController extends Controller
{

    public function __construct()
    {
        parent::__construct();

        // special authentication check for the entire controller: Note the check-ADMIN-authentication!
        // All methods inside this controller are only accessible for admins (= users that have role type 7)
       // Auth::checkSellerAuthentication();
    }
 
    /**
     * This method controls what happens when you move to /admin or /admin/index in your app.
     */
    public function index()
    {
        $this->View->renderFrontWithoutHeadFoot('vent/index', array(
                'users' => UserModel::getPublicProfilesOfAllUsers())
        );
    }
      
    
    /**
     * The add vent in database
     */
    public static function addVent()
    {
        $vent_successful = VentModel::addtheVent();
       
        
    }
       
}
