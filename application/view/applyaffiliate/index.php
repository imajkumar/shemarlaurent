 <section class="seller_apply" style="background-image: linear-gradient(to right, #fe6034, #fff754);">
        <div class="container">
            <div class="content">
                <h2>Apply Now</h2>
                <form action="#">
                    <div class="input_area">
                        <label for="">Name</label>
                        <input type="text" id="name" name="name" required>
                    </div>

                    <div class="input_area">
                        <label for="">Email</label>
                        <input type='email' id="email" name="email" required>
                    </div>

                    <div class="input_area">
                        <label for="">Social Media @ Name</label>
                        <input type="text" id="socialName" name="socialName" required>
                    </div>

                    <div class="input_area">
                        <label for="">No of followers</label>
                        <input type="text" id="noFollower" name="noFollower" required>
                    </div>

                    <div class="input_area">
                        <label for="">Do you have an active youtube?</label>
                        <div class="radio"><span>Y</span> <br> <input type="radio" name="followers" value="1" checked id="fyes"></div>
                        <div class="radio"><span>N</span> <br> <input type="radio" name="followers" value="0" id="fno"></div>
                    </div>

                    <div class="input_area followers">
                        <label for="">No of youtube subscribers</label>
                        <input type="text" name="youtubeSubscriberNo" id="youtubeSubscriberNo" required>
                    </div>

                    <div class="input_area">
                        <label for="">Tell us about yourself</label>
                        <textarea name="message" name="message" id="message"></textarea>
                    </div>
                    <button type="button" id="affiliateApply" value="submit">apply</button>
                </form>
            </div>
        </div>
    </section>



    <!-- loader area -->
    <section class="loader">
        <div class="container">
            <div class="content text-center">
                <img src="<?=Config::get('URL'); ?>front/images/loader.PNG" alt="loader">
                <h5>Please wait, while we create your dashboard</h5>
                <p>Remebber to fill in the mecessary fields in the settings before deploying your code</p>
            </div>
        </div>
    </section>
    <!-- end loader area -->