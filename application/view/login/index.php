<div class="container">

    <!-- echo out the system feedback (error and success messages) -->
    <?php $this->renderFeedbackMessages(); ?>

    <div class="login-page-box">
        <div class="table-wrapper" style="margin: 40px 0">

            <div class="col-md-8">
                <div class="card">
                    <h2 class="card-header">Login here</h2>
                    <div class="card-body">
         
                        <form action="<?php echo Config::get('URL'); ?>login/login" method="post">
                            <div class="form-group row">
                                <input class="form-control" type="text" name="user_name" placeholder="Username or email" required />
                            </div>
                            <div class="form-group row">
                                <input class="form-control" type="password" name="user_password" placeholder="Password" required />
                            </div>
                            <div class="form-group row">
                                <label for="set_remember_me_cookie" class="remember-me-label">
                                    <input type="checkbox" name="set_remember_me_cookie" class="remember-me-checkbox" />
                                    Remember me for 2 weeks
                                </label>
                            </div>
                            <?php if (!empty($this->redirect)) { ?>
                                <input type="hidden" name="redirect" value="<?php echo $this->encodeHTML($this->redirect); ?>" />
                            <?php } ?>
                            <!--
                                set CSRF token in login form, although sending fake login requests mightn't be interesting gap here.
                                If you want to get deeper, check these answers:
                                    1. natevw's http://stackoverflow.com/questions/6412813/do-login-forms-need-tokens-against-csrf-attacks?rq=1
                                    2. http://stackoverflow.com/questions/15602473/is-csrf-protection-necessary-on-a-sign-up-form?lq=1
                                    3. http://stackoverflow.com/questions/13667437/how-to-add-csrf-token-to-login-form?lq=1
                            -->
                            <input type="hidden" name="csrf_token" value="<?= Csrf::makeToken(); ?>" />
                            <input type="submit" class="btn btn-secondary btn-sm login-submit-button" value="Log in"/>
                        </form>
                        <div class="link-forgot-my-password">
                            <a href="<?php echo Config::get('URL'); ?>login/requestPasswordReset">I forgot my password</a>
                        </div>
                    </div>

                    <!-- register box on right side -->
                    <div class="card-footer">
                        <p class="card-title">Not register yet ?
                        <a href="<?php echo Config::get('URL'); ?>register/index">Register</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
