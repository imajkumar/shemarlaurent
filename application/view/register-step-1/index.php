 <!--- header area -->
 <section class="header">
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?=Config::get('URL'); ?>">back <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->


    <!-- register -->
    <section class="register reg2">
        <div class="container text-center">
            <div class="content content22">
                <h6>You're just couple steps closer away from having hyped items in your closet...</h6>
                <div class="bar101">
                    <img src="<?=Config::get('URL'); ?>front/images/5.PNG" alt="" class="img1">
                    <div class="bar">
                        <div class="overlay"></div>
                    </div>
                    <img src="<?=Config::get('URL'); ?>front/images/6.PNG" alt="" class="img2">
                </div>
                <?php
               
                $plan_info=RegistrationStep2Controller::getMemberShipPlans_details();
               

              
                $member_plans=RegistrationStep2Controller::getMemberShipPlans();
               

                 ?>
                <form action="#">
                    <div class="box boxx">
                        <div class="check">
                            <span><i class="far fa-check-circle"></i></span>
                            <h6>STEP 2 OF 3</h6>
                            <h3>Choose the plan that’s right for you</h3>
                        </div>
                        <table>
                                <tr id="planSelected">
                                   <td></td>
                                    <td id="planMemberbasic" class="basic">
                                        <div class="ptitle">
                                            <h5 class="planname" id="<?=$member_plans['plan_data'][1]->member_id?>"><?=$member_plans['plan_data'][1]->member_type?></h5>
                                        </div>
                                    </td>
                                    <td id="planMemberstandard" class="standard">
                                        <div class="ptitle">
                                            <h5 class="planname"  id="<?=$member_plans['plan_data'][2]->member_id?>" ><?=$member_plans['plan_data'][2]->member_type?></h5>
                                        </div>
                                    </td>
                                    <td id="planMemberpremium" class="premium">
                                        <div class="ptitle">
                                            <h5 class="planname"  id="<?=$member_plans['plan_data'][3]->member_id?>" ><?=$member_plans['plan_data'][3]->member_type?></h5>
                                        </div>
                                    </td>
                                </tr>
                                <?php 
                                
                                foreach ($plan_info as $key => $rowData) {
                                    foreach ($rowData as $key1 => $row) {
                                       
                                             
                                             
                                              
                                        
                                        ?>
                                         <tr>
                                            <td><?=$row->name?></td>
                                           
                                            <?php
                                             foreach ($row->plan_data as $key2 => $keyRow) {
                                               
                                                ?>
                                                <td class="standard"><span><?=$keyRow->plan_value?></span></td>
                                                <?php
                                              }
                                            ?>
                                            
                                           
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                               
                                <!-- <tr>
                                   <td>HD available</td>
                                    <td class="basic"><span><i class="fas fa-times"></i></span></td>
                                    <td class="standard"><span><i class="fas fa-check"></i></span></td>
                                    <td><span><i class="fas fa-check"></i></span></td>
                                </tr>
                                <tr>
                                    <td>Ultra HD available</td>
                                    <td class="basic"><span><i class="fas fa-times"></i></span></td>
                                    <td class="standard"><span><i class="fas fa-times"></i></span></td>
                                    <td><span><i class="fas fa-check"></i></span></td>
                                </tr>
                                <tr>
                                    <td>Screens you can watch on at the same time</td>
                                    <td class="basic"><span>1</span></td>
                                    <td class="standard"><span>2</span></td>
                                    <td><span>4</span></td>
                                </tr>
                                <tr>
                                    <td>Watch on your laptop, TV, phone and tablet</td>
                                    <td class="basic"><span><i class="fas fa-times"></i></span></td>
                                    <td class="standard"><span><i class="fas fa-times"></i></span></td>
                                    <td><span><i class="fas fa-check"></i></span></td>
                                </tr>
                                <tr>
                                    <td>Watch on your laptop, TV, phone and tablet</td>
                                    <td class="basic"><span><i class="fas fa-check"></i></span></td>
                                    <td class="standard"><span><i class="fas fa-check"></i></span></td>
                                    <td><span><i class="fas fa-check"></i></span></td>
                                </tr>
                                <tr>
                                    <td>Unlimited movies and TV shows</td>
                                    <td class="basic"><span><i class="fas fa-check"></i></span></td>
                                    <td class="standard"><span><i class="fas fa-check"></i></span></td>
                                    <td class="premium"><span><i class="fas fa-check"></i></span></td>
                                </tr> -->
                            </table>
                        <a id="serviceOption" style="color:#fff; cursor:pointer">continue</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- end register -->
