<div class="col-md-9">
        <!-- overview -->
        <div class="overview-content">
            <!-- search bar -->
            <div class="search_bar" style="height: auto;">
                <h3>products</h3>
                <div class="row no-gutters">
                    <div class="col-lg-8 col-md-6">
                        <div class="search search-bar">
                            <form action="#">
                                <input type="search" name="search" id="search">
                                <button type="submit">search</button>
                            </form>
                            <div class="search_content">
                                <!-- images -->
                                <div class="images">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- content -->
                                            <div class="cont">
                                                <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                            </div>
                                            <!-- end content -->
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                                <div class="col-md-4 col-6">
                                                    <!-- content -->
                                                    <div class="cont">
                                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                                    </div>
                                                    <!-- end content -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end images -->
                                <!-- fields -->
                                <div class="fields">
                                    <form action="#">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <!-- input field -->
                                                <input type="hidden" name="productId" id="productId">
                                                <div class="input-field">
                                                    <label for="#">Brand Name</label>
                                                    <input type="text" name="brand_name" id="brand_name" readonly>
                                                </div>
                                                <!-- end input field -->
                                                <!-- input field -->
                                                <div class="input-field">
                                                    <label for="#">Product Name</label>
                                                    <input type="text" name="product_name" id="product_name" required="">
                                                </div>
                                                <!-- end input field -->
                                            </div>
                                            <div class="col-md-6">
                                                <!-- input field -->
                                                <div class="input-field">
                                                    <label for="#">Old Price</label>
                                                    <input type="number" name="old_price" id="old_price" required="">
                                                </div>
                                                <!-- end input field -->
                                                <!-- input field -->
                                                <div class="input-field">
                                                    <label for="#">New Price</label>
                                                    <input type="number" name="new_price" id="new_price" required="">
                                                </div>
                                                <!-- end input field -->
                                            </div>
                                            <div class="col-md-12">
                                                <!-- button -->
                                                <div class="button">
                                                    <button type="button" id="updateProduct" name="updateProduct">Update</button>
                                                </div>
                                                <!-- end button -->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- end fields -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="buttons">
                            <button type="button">generate</button>
                            <button type="button">remove</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end search bar -->
        </div>
                <!-- end overview -->

                <!-- affaliate grid -->
                <div class="affaliate_grid admin-orders">
                    <div class="row">
                    <?php
                        $products=$this->products;
                        if(count($products)>0){
                            foreach ($products as $key => $rowData) {
                             

                    ?>
                    <div class="col-lg-3 col-md-4 col-sm-4">
                            <!-- content -->
                            <div class="content" product_id = "<?=$rowData->id?>">
                                <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                <div class="cont">
                                    <div class="carousel101">
                                        <div class="owl-carousel owl-theme">
                                            <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                            <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                            <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                        </div>                                       
                                        <div class="text">
                                            <ul>
                                                <li id="brandName" brandName = "<?=$rowData->brand?>">Brand name : <?=$rowData->brand?></li>
                                                <li id="productName" productName = "<?=$rowData->product_title?>">Product name : <?=$rowData->product_title?></li>
                                                <li id="sellerPrice" sellerPrice = "<?=$rowData->admin_price?>">Price : <?=$rowData->admin_price?></li>
                                                <li >Size : <?=$rowData->sizes?></li>
                                                <li Weight : <?=$rowData->weight?></li>
                                                <li >SKU : <?=$rowData->product_sku?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <h5><?=$rowData->product_title?></h5>
                                <div></div>
                            </div>
                            <div class="del10">
                                <div class="box"><span><i class="fas fa-times"></i></span></div>
                            </div>
                            <!-- end content -->
                        </div>
                    <?php
                }
            }
            ?>
                        
                        
                        
                    </div>
                </div>
                <!-- end affaliate grid -->
            </div>
        </div>
    </div>
</section>
<!-- end main  content -->





    
<div class="soverlay"></div>
