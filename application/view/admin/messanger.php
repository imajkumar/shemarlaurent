<div class="col-md-9">
    <!-- admin messenger -->
    <div class="admin-messenger">
        <!-- bar -->
        <div class="bar">
            <div class="row">
                <div class="col-sm-1">
                    <div class="image">
                        <img src="<?=Config::get('URL'); ?>backend/images/19.PNG" alt="">
                    </div>
                </div>
                <div class="col-sm-11">
                    <div class="owl-carousel owl-theme" id="chatUsers">
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- end bar -->

        <!-- chat bar -->
        <div class="chat">
            <div class="c-content" id="chat_area" style="padding:10px; max-height:420px; overflow-y:scroll;">
               
            </div>
            <div class="typing"><span><i class="fas fa-ellipsis-h"></i></span></div>
            <div class="send-bar">
                <div class="row">
                    <div class="col-lg-3 col-md-4">
                        <div class="icon">
                            <ul>
                                <li><img src="images/17.PNG" alt="">
                                </li>
                                <li><img src="images/18.PNG" alt=""></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8">
                        <div class="send">
                            <form action="#" onSubmit="return false">
                                <input type="text" name="chat_msg" id="chat_msg">
                                <input type="hidden" name="chatagent" id="chatagent" value="<?php echo $_REQUEST['chatUser'] ?>">
                                <button type="button" id="send_msg">send</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end chat bar -->
    </div>
    <!-- end admin messenger -->
</div>