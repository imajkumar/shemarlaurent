<div class="col-md-9">
        <!-- affaliate settings -->
        <div class="affaliate_settings">
            <a href="#"><span><i class="fas fa-long-arrow-alt-left"></i></span> back</a>
            <div class="row">
                <div class="col-md-5">
                    <!-- text -->
                    <div class="text">
                        <h6>Your Details</h6>
                        <p>This details are to help us contact you encase of an emergancy</p>
                    </div>
                    <!-- end text -->
                </div>
                <div class="col-md-7">
                    <!-- form -->
                    <form action="#">
                        <div class="content">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="input-field">
                                        <label for="name">Store Name</label>
                                        <input type="text" name="name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="input-field">
                                        <label for="email">Store Email</label>
                                        <input type="email" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <p>This information will be saved for a serious emergencies</p>
                        </div>
                        <div class="button"><button type="submit">Save</button></div>
                    </form>
                    <!-- end form -->
                </div>
            </div>
        </div>
        <!-- end affaliate settings -->
    </div>