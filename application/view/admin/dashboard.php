<div class="col-md-9">
    <!-- dashbord overview -->
    <div class="dashbord_overview">
        <h3>dashbord overview</h3>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo8"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo9"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo10"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo11"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo12"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo13"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo14"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo15"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h5>wordlwide usage</h5>
                    <table>
                        <tr>
                            <td class="brnad">USA</td>
                            <td>
                                <h6>450</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Canada</td>
                            <td>
                                <h6>340</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Europe</td>
                            <td>
                                <h6>200</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Mexico</td>
                            <td>
                                <h6>120</h6>
                            </td>
                            <td>
                                <p><span class="up down"><i class="fas fa-long-arrow-alt-down"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Japan</td>
                            <td>
                                <h6>720</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                            <tr>
                            <td class="brnad">Austrllia</td>
                            <td>
                                <h6>720</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h5>top current products</h5>
                    <table>
                        <tr>
                            <td class="brnad">Product1</td>
                            <td>
                                <p>#1</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product2</td>
                            <td>
                                <p>#2</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product3</td>
                            <td>
                                <p>#3</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product4</td>
                            <td>
                                <p>#4</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product5</td>
                            <td>
                                <p>#5</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product6</td>
                            <td>
                                <p>#6</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Product7</td>
                            <td>
                                <p>#7</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h6>total orders</h6>
                    <h4>4 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo16"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #000;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end overview -->
            </div>
            <div class="col-md-4 col-sm-6">
                <!-- overview -->
                <div class="overview">
                    <h5>wordlwide usage</h5>
                    <table>
                        <tr>
                            <td class="brnad">USA</td>
                            <td>
                                <h6>450</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Canada</td>
                            <td>
                                <h6>340</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Europe</td>
                            <td>
                                <h6>200</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Mexico</td>
                            <td>
                                <h6>120</h6>
                            </td>
                            <td>
                                <p><span class="up down"><i class="fas fa-long-arrow-alt-down"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Japan</td>
                            <td>
                                <h6>720</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end overview -->
            </div>
        </div>
    </div>
    <!-- end dashbord overview -->
</div>