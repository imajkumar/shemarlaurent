<div class="col-md-9">
    <!-- admin orders -->
    <div class="admin-orders">
        <!-- search bar -->
        <div class="search_bar">
            <!-- search -->
            <div class="search">
                <form action="#">
                    <input type="search" name="search">
                    <button type="submit">Search</button>
                </form>
                <!-- search content -->
                <div class="search_content">
                    <!-- images -->
                    <div class="images">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- content -->
                                <div class="cont">
                                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                </div>
                                <!-- end content -->
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                    <div class="col-md-4 col-6">
                                        <!-- content -->
                                        <div class="cont">
                                            <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        </div>
                                        <!-- end content -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end images -->
                    <!-- fields -->
                    <div class="fields">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="#">Brand Name</label>
                                        <input type="text" name="brand_name" required>
                                    </div>
                                    <!-- end input field -->
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="#">Product Name</label>
                                        <input type="text" name="brand_name" required>
                                    </div>
                                    <!-- end input field -->
                                </div>
                                <div class="col-md-6">
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="#">Old Price</label>
                                        <input type="text" name="brand_name" required>
                                    </div>
                                    <!-- end input field -->
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="#">New Price</label>
                                        <input type="text" name="brand_name" required>
                                    </div>
                                    <!-- end input field -->
                                </div>
                                <div class="col-md-12">
                                    <!-- button -->
                                    <div class="button">
                                        <button type="button"><a href="seller-add-product.html">upload</a></button>
                                    </div>
                                    <!-- end button -->
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end fields -->
                </div>
                <!-- end search content -->
            </div>
            <!-- end search -->
            <!-- orders -->
                                <!-- affaliate grid -->
    <div class="affaliate_grid admin-orders">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="" alt=""></div>
                                <div class="item"><img src="" alt=""></div>
                                <div class="item"><img src="" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4">
                <!-- content -->
                <div class="content">
                    <div class="box"></div>
                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                    <div class="cont">
                        <div class="carousel101">
                            <div class="owl-carousel owl-theme">
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                                <div class="item"><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></div>
                            </div>

                            <div class="text">
                                <ul>
                                    <li>brand name</li>
                                    <li>product name</li>
                                    <li>price</li>
                                    <li>size</li>
                                    <li>weight</li>
                                    <li>SKU</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="del10">
                    <div class="box"><span><i class="fas fa-times"></i></span></div>
                </div>
                <!-- end content -->
            </div>
        </div>
    </div>
    <!-- end affaliate grid -->
            <!-- end orders -->
        </div>
        <!-- end search bar -->
    </div>
    <!-- end admin orders -->
</div>