<div class="col-md-9">
                    <!-- overview -->
                    <div class="overview-content">
                        <!-- search bar -->
                        <div class="search_bar" style="height: auto;">
                            <h3>Affiliate Overview</h3>
                            <div class="row no-gutters">
                                <div class="col-lg-8 col-md-6">
                                    <div class="search">
                                        <form action="#">
                                            <input type="search" name="search">
                                            <button type="submit">search</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="buttons">
                                        <button type="button" id="btnSellerGenCode">generate</button>
                                        <button type="button">remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end search bar -->
                    </div>
                    <!-- end overview -->
                    <?php
                        $affiliate_data = $this->affiliate_data;                   
                        
                    ?>
                    <!-- affaliate grid -->
                    <div class="affaliate_grid">
                        <div class="row">
                        <?php
                        foreach ($affiliate_data as $key => $row) {
                           ?>
                            <div class="col-lg-3 col-md-4 col-sm-4">
                                <!-- content -->
                                <div class="content">
                                    <div class="box"><img src="images/1.jpg" alt=""></div>
                                    <h6><?=$row->name?> <?=$row->email?></h6>
                                </div>
                                <a id="<?=$row->id?>" class="affiliateApproved" href="javascript:void(0)">Approved</a> |  <a href="javascript:void(0)">Decline</a>
                                <!-- end content -->
                            </div>
                           <?php
                        } 
                        ?>
                           
                            
                           
                        </div>
                    </div>
                    <!-- end affaliate grid -->
                </div>
            </div>
        </div>
    </section>
    <!-- end main  content -->



    <!-- code jenerator -->
    <div class="first-sale admin_code">
        <div class="overlay" style="display: block;"></div>
        <div class="content">
            <h6>With great power comes Great Responsibility, use the code wisely</h6>
            <form action="#">
                <div class="input">
                    <input type="text" name="key">
                    <a href="#"><img src="images/20.PNG" alt=""></a>
                </div>
                <button type="reset" class="cencel">Cencel</button>
                <button type="submit">Activate</button>
            </form>
        </div>
    </div>
    <!-- end code jenerator -->
