    <!--- header area -->
    <section class="header">
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="index.html">back <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->



    <!-- registration form -->
    <section class="registration_area">
        <div class="container">
            <form action="#">
                <h5>Welcome, create your account, <br> and start earning more with less</h5>
                <!-- input area -->
                <div class="input_area">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="d-block" required>
                </div>
                <!-- end input area -->
                <!-- input area -->
                <div class="input_area">
                    <label for="Email">Email</label>
                    <input type="email" name="email" id="email" class="d-block" required>
                </div>
                <!-- end input area -->
                <!-- input area -->
                <div class="input_area">
                    <label for="pass">Password</label>
                    <input type="password" name="pass" id="pass" class="d-block" required>
                </div>
                <!-- end input area -->
                <!-- input area -->
                <div class="input_area">
                    <label for="store_name">Store Name</label>
                    <input type="text" name="store_name" id="store_name" class="d-block" required>
                </div>
                <!-- end input area -->
                <button type="button" id="cerateAccount">Create Account</button>
            </form>
        </div>
    </section>
    <!-- end registration form -->
    
    
    
    <!-- loader area -->
    <section class="loader">
        <div class="container">
            <div class="content text-center">
                <img src="<?=Config::get('URL'); ?>front/images/loader.PNG" alt="loader">
                <h5>Please wait, while we create your dashboard</h5>
                <p>Remebber to fill in the mecessary fields in the settings before upload your first item </p>
            </div>
        </div>
    </section>
    <!-- end loader area -->




