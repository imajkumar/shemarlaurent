   <!--- header area -->
   <section class="header">
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?=Config::get('URL'); ?>">back <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->
    
    
    <!-- vent area -->
    <section class="vent_area">
        <div class="container">
            <div class="content">
               <h6>Tell us the problem you wished were fixed in the streetwear/Fasion communicaty</h6>
                <form action="#">
                    <textarea name="vent_message" id="ventMessage" placeholder="Vent Here" required></textarea>
                    <button type="button" id="ventBtn">Submit</button>
                </form>
            </div>
        </div>
    </section>
    <!-- end vent area -->
    
    
    
   
    