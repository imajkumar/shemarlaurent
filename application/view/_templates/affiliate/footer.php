
<!-- js link -->

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- font awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <!-- custom js -->
    
    <script src="<?=Config::get('URL'); ?>backend/js/blueberryCharts.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/owl.carousel.js"></script>
    <script src="<?=Config::get('URL'); ?>front/js/toastr.min.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/custom.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/affiliate.js"></script>
    <script type="text/javascript">
    BASE_URL=$('meta[name="BASE_URL"]').attr('content');

</script>
</body>

</html>
