<!-- js link -->

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <!-- font awesome -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <!-- custom js -->
    <script type="text/javascript">
			 aBASE_URL=$('meta[name="BASE_URL"]').attr('content');
			 BASE_URL=$('meta[name="BASE_URL"]').attr('content');
		</script>
    <script src="<?=Config::get('URL'); ?>backend/js/blueberryCharts.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/owl.carousel.js"></script>
    <script src="<?=Config::get('URL'); ?>front/js/toastr.min.js"></script>
    <script type="text/javascript" src="<?=Config::get('URL'); ?>backend/js/dropzone.js"></script>
    <script>
    $(document).ready(function(){
	$(".dropzoneu").dropzone({
	  url: aBASE_URL+'product/updateProductIMG',
	  width: 300,
	  height: 300, 
	  progressBarWidth: '100%',
	  maxFileSize: '5MB'
	})
});
    </script>
    <script src="<?=Config::get('URL'); ?>backend/js/custom.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/seller.js"></script>
    <script src="<?=Config::get('URL'); ?>backend/js/sellerchat.js"></script>
    
    
</body>

</html>
