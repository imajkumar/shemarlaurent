<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="BASE_URL" content="<?=Config::get('BASE_URL')?>" >
    <meta name="csrf_token" content="<?= Csrf::makeToken(); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="88ninety">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Helyos</title>
    <link href="image/favicon.png" rel="shortcut icon" type="image/png">
    <!-- ================css link=================== -->
    <!--fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&display=swap" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- custom css -->
    
    <link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/toastr.min.css">
    <link href="<?=Config::get('URL'); ?>backend/css/style.css" rel="stylesheet">
    <!--responsive css-->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>backend/css/responsive.css">
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>backend/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>backend/css/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?=Config::get('URL'); ?>backend/css/dropzone.css" />
     


    <style>
        .admin_home .bar {
            background: #fff;
            padding: 15px;
            position: relative;
        }

        .admin_home .bar .text {
            max-width: 35rem;
        }

        .admin_home .bar .cross {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            right: 20px;
            width: 25px;
            cursor: pointer;
        }
        /* Bootstrap Notify ============================ */
.bootstrap-notify-container {
  max-width: 320px;
  text-align: center; 
  }

    </style>
</head>

<body>
    <!-- top bar -->
    <section class="top-bar d-none d-lg-block d-md-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    <div class="logo">
                        <a href="#"><img src="<?=Config::get('URL'); ?>backend/images/logo.PNG" alt=""></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="search">
                        <form action="#">
                            <input type="search" name="search">
                            <button type="submit">search</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="profile">
                        <div class="image">
                            <img src="<?=Config::get('URL'); ?>backend/images/profile.png" alt="">
                            <ul>
                                <li><a href="<?=Config::get('URL'); ?>seller/myaccount"><span><i class="far fa-user"></i></span> my account</a></li>
                                <li><a href="<?php echo Config::get('URL'); ?>login/logout"><span><i class="fas fa-sign-out-alt"></i> log out</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- mobile -->
    <div class="mobile-bar d-block d-lg-none d-md-none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    <div class="menu_icon">
                        <img src="<?=Config::get('URL'); ?>backend/images/menu-alt-512.png" alt="">
                    </div>
                </div>
                <div class="col-8">
                    <div class="search">
                        <form action="#">
                            <input type="search" name="search">
                            <button type="submit"><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-2">
                    <div class="profile">
                        <div class="image">
                            <img src="<?=Config::get('URL'); ?>backend/images/profile.png" alt="">
                            <ul>
                                <li><a href="<?=Config::get('URL'); ?>myaccount"><span><i class="far fa-user"></i></span> my account</a></li>
                                <li><a href="#"><span><i class="fas fa-sign-out-alt"></i> log out</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end mobile -->
    <!-- end top bar -->



    <!-- main  content -->
    <section class="main-content">
        <div class="moverlay d-block d-lg-none d-md-none"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <!-- menu -->
                    <div class="menu">
                        <ul>
                            <li class="active"><a href="<?=Config::get('URL'); ?>seller"><img src="<?=Config::get('URL'); ?>backend/images/1.png" alt=""> home</a></li>
                            <li><a href="<?=Config::get('URL'); ?>seller/sellerorder"><img src="<?=Config::get('URL'); ?>backend/images/3.png" alt=""> orders</a></li>
                            <li><a href="<?=Config::get('URL'); ?>seller/selleranalytics"><img src="<?=Config::get('URL'); ?>backend/images/14.png" alt=""> analytics</a></li>
                            <li><a href="<?=Config::get('URL'); ?>seller/products"><img src="<?=Config::get('URL'); ?>backend/images/5.png" alt=""> products</a></li>
                            <li><a href="<?=Config::get('URL'); ?>sellermessanger"><img src="<?=Config::get('URL'); ?>backend/images/7.png" alt=""> messenger</a></li>
                            <li class="settings"><a href="<?=Config::get('URL'); ?>seller/sellersettings"><img src="<?=Config::get('URL'); ?>backend/images/10.png" alt=""> settings</a></li>
                        </ul>
                    </div>
                    <!-- end menu -->
                </div>
