		<!-- js link -->

		<!-- jquery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- bootstrap -->
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<!-- font awesome -->
		<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
		<!-- high chart -->
		<script src="<?=Config::get('URL'); ?>backend/js/toastr.min.js"></script>    
		

		<script type="text/javascript">
			BASE_URL=$('meta[name="BASE_URL"]').attr('content');
		</script>
		<!-- high chart -->
		<script src="<?=Config::get('URL'); ?>backend/js/owl.carousel.js"></script>
		<script src="<?=Config::get('URL'); ?>backend/js/blueberryCharts.js"></script>
		<!-- custom js -->	
		<script src="<?=Config::get('URL'); ?>backend/js/custom.js"></script>
		<script src="<?=Config::get('URL'); ?>backend/js/adminchat.js"></script>
	</body>
	<!--begin::Modal-->
	<div class="modal fade" id="kt_modal_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Seller Code</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					
						
						<div class="form-group">
							<label for="recipient-name" class="form-control-label">Code:</label>
							<input type="text" class="form-control" id="txtUNCode">
						</div>
						<div class="form-group">
							<label for="message-text" class="form-control-label">Message:</label>
							<textarea class="form-control" id="message-text"></textarea>
						</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" id="btnGenerateSellercode" class="btn btn-secondary" >Generate</button>
					<button type="button" id="btnSaveAuthCode" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>
	<!--end::Modal-->
</html>
