<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="BASE_URL" content="<?=Config::get('BASE_URL')?>" >
    <meta name="csrf_token" content="<?= Csrf::makeToken(); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="88ninety">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Helyos</title>
    <link href="image/favicon.png" rel="shortcut icon" type="image/png">
    <!-- ================css link=================== -->
    <!--fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&display=swap" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/owl.theme.default.css">

    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/toastr.min.css">
       
    <!-- end owl carousel -->
    <!-- custom css -->
    <link href="<?=Config::get('URL'); ?>front/css/style.css" rel="stylesheet">
    <!--responsive css-->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/responsive.css">
</head>

<body>

    <!--- header area -->
    <section id="hideheader" class="header">
        <!-- top bar -->
        <div class="top-bar text-center">
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <h6>Welcome to Streetwear Paradise</h6>
                </div>
                <div class="item">
                    <h6>App comming soon !!</h6>
                </div>
            </div>
            <div class="delete-btn">
                <span><i class="fas fa-times"></i></span>
            </div>
        </div>
        <!-- end top bar -->
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <img src="<?=Config::get('URL'); ?>front/images/menu-alt-512.png" alt="">
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item affalite_btn d-none d-lg-block">
                                <a class="nav-link" href="#">affalite <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item seller_btn d-none d-lg-block">
                                <a class="nav-link" href="#">seller</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?=Config::get('URL'); ?>registrationstep">register</a>
                            </li>
                            <li class="nav-item signin">
                                <a class="nav-link" href="#">login</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->

