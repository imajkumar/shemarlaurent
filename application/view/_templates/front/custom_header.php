<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="BASE_URL" content="<?=Config::get('BASE_URL')?>" >
    <meta name="csrf_token" content="<?= Csrf::makeToken(); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="88ninety">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Helyos</title>
    <link href="image/favicon.png" rel="shortcut icon" type="image/png">
    <!-- ================css link=================== -->
    <!--fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700&display=swap" rel="stylesheet">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- font awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/owl.carousel.css">
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/owl.theme.default.css">

    <!-- owl carousel -->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/toastr.min.css">
       
    <!-- end owl carousel -->
    <!-- custom css -->
    <link href="<?=Config::get('URL'); ?>front/css/style.css" rel="stylesheet">
    <!--responsive css-->
    <link rel="stylesheet" href="<?=Config::get('URL'); ?>front/css/responsive.css">
</head>

<body>
