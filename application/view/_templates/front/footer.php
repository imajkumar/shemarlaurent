<!-- login area -->
<section class="login">
    <div class="overlay"></div>
    <div class="content">
        <form action="#">
            <!-- google btn -->
            <div class="google-btn">
                <a href="#">
                    <img src="<?=Config::get('URL'); ?>front/images/google_btn.png" alt="gbtn">
                    <span>Sign in with Google</span>
                </a>
            </div>
            <!-- end google btn -->
            <h5>Welcome back to Paradise</h5>
            <!-- input area -->
            <div class="input_area">
                <label for="email">Email Address</label>
                <input type="text" name="user_name" id="user_name"  autocomplete="off" required class="d-block">
            </div>
            <!-- end input area -->
            <!-- input area -->
            <div class="input_area">
                <label for="pass">Password</label>
                <input type="password" name="user_password" id="user_password" required class="d-block">
            </div>
            <!-- end input area -->
            <a href="#" class="forget_pass">Forgot Password</a>
            <button type="button" id="btnLogin">Sign in</button>
            <div class="check">
                <input type="checkbox" name="vehicle1" value="Bike">
                <span>Keep me sign in until I sign out</span>
            </div>
            <a href="<?=Config::get('URL')?>register" class="new_sign_up">New here? Sign up</a>
        </form>
    </div>
</section>
<!-- end login area -->

    <!-- affiliate -->
    <section class="affiliate">
        <div class="overlay"></div>
        <div class="content">
            <form action="#">
                <h2>Enter Affiliate Code</h2>
                <input type="text" required>
                <button type="submit">enter</button>
            </form>
            <a href="<?=Config::get('URL')?>applyaffiliate">Apply Now</a>
        </div>
    </section>
    <!-- end affiliate -->



    <!-- seller -->
    <section class="seller">
        <div class="overlay"></div>
        <div class="content">
            <form action="#">
                <h2>Enter Seller code</h2>
                <input type="text" required id="txtSellerCode">
                <button type="submit" id="btnSellerEnter">enter</button>
            </form>
            <a class="sellerApply" href="<?=Config::get('URL'); ?>SellerApply/index">Apply Now</a>
        </div>
    </section>
    <!-- end affiliate -->



    <!-- stall area -->
    <section class="stall">
        <div class="overlay"></div>
        <div class="content text-center">
            <h3>We're Comming SOON!!</h3>
            <h5>Soon we will make it rain with all the hyped ish, until then spread the world and let the streets know we comming</h5>
            <h4>Exclusive Access</h4>
            <div class="buttons">
                <a href="#">affiliate</a>
                <a href="#">seller</a>
            </div>
            <form action="#">
                <h6>Stay updated, enter your email</h6>
                <input type="email" name="subs_email" id="subs_email">
                <button type="button" id="subscribe" name="subscribe">send</button>
            </form>
        </div>
    </section>
    <!-- end stall area -->


<!-- footer area -->
<section id="hidefooter" class="footer">
   <div class="container">
       <div class="vent">
           <h6>Do you have any problems with the streetwear/Fasion communicaty you wish were fixed? Let us know!</h6>
           <button type="button"><a href="<?=Config::get('URL'); ?>vent">vent</a></button>
       </div>
   </div>
   <hr>
   <div class="container-fluid">
       <div class="content">
           <div class="row">
               <div class="col-lg-7 col-md-6">
                   <div class="nav">
                       <ul>
                           <li><a href="#">faq</a></li>
                           <li><a href="#">about us</a></li>
                           <li><a href="#">contact</a></li>
                           <li><a href="#">terms</a></li>
                           <li><a href="#">privacy</a></li>
                       </ul>
                   </div>
               </div>
               <div class="col-lg-5 col-md-6">
                   <div class="row">
                       <div class="col-md-6 col-sm-6">
                           <div class="app_icon">
                               <ul>
                                   <li>Get the app</li>
                                   <li><a href="#"><i class="fab fa-apple"></i></a></li>
                                   <li><a href="#"><i class="fab fa-android"></i></a></li>
                               </ul>
                           </div>
                       </div>
                       <div class="col-md-6 col-sm-6">
                           <div class="social_icon">
                               <ul>
                                   <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                   <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                   <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                   <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</section>
<!-- end footer area -->


<!-- copyright -->
<section class="copyright">
   <div class="container text-center">
       <h6>&copy; Helyos, All right reserved.</h6>
   </div>
</section>
<!-- end copyright -->






<!-- js link -->

<!-- jquery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- bootstrap -->
<script src="<?=Config::get('URL'); ?>front/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<!-- font awesome -->
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script type="text/javascript">
 BASE_URL=$('meta[name="BASE_URL"]').attr('content');

</script>

<script>



// $(document).ready(function(){
 
//  var limit = 7;
//  var start = 0;
//  var action = 'inactive';
//  function load_country_data(limit, start)
//  {
//   $.ajax({
//    url:BASE_URL+'/product/fetchAll',
//    method:"POST",
//    data:{limit:limit, start:start},
//    cache:false,
//    success:function(data)
//    {
//     $('#load_data').append(data);
//     if(data == '')
//     {
//      $('#load_data_message').html("<button type='button' class='btn btn-info'>No Data Found</button>");
//      action = 'active';
//     }
//     else
//     {
//      $('#load_data_message').html("<button type='button' class='btn btn-warning'>Please Wait....</button>");
//      action = "inactive";
//     }
//    }
//   });
//  }

//  if(action == 'inactive')
//  {
//   action = 'active';
//   load_country_data(limit, start);
//  }
//  $(window).scroll(function(){
//   if($(window).scrollTop() + $(window).height() > $("#load_data").height() ')
//   {
//       alert(4545);
//    action = 'active';
//    start = start + limit;
//    setTimeout(function(){
//     load_country_data(limit, start);
//    }, 1000);
//   }
//  });
 
// });

</script>



<!-- owl carousel -->
<script src="<?=Config::get('URL'); ?>front/js/owl.carousel.js"></script>
<!-- custom js -->
<script src="<?=Config::get('URL'); ?>front/js/front.js"></script>
</body>


</html>
