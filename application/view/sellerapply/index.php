
    <section class="seller_apply">
        <div class="container">
            <div class="content">
                <h2>Apply Now</h2>
                <form action="#">
                    <div class="input_area">
                        <label for="">Business Name</label>
                        <input type="text" id="name" id="name" required>
                    </div>

                    <div class="input_area">
                        <label for="">Website</label>
                        <input type='url' id="website" name="website" required>
                    </div>

                    <div class="input_area">
                        <label for="">Email</label>
                        <input type="email" id="email" name="email" required>
                    </div>

                    <div class="input_area">
                        <label for="">Are you locate in the US?</label>
                        <div class="radio"><span>Y</span> <br> <input type="radio" name="location" value="1" checked id="yes"></div>
                        <div class="radio"><span>N</span> <br> <input type="radio" name="location" value="0" id="no"></div>
                    </div>

                    <div class="input_area what_city">
                        <label for="">What City</label>
                        <input type="text" name="city" id="city" required>
                    </div>

                    <div class="input_area">
                        <label for="">Do you ship?</label>
                        <div class="radio"><span>Y</span> <br> <input type="radio" name="ship" id="shipyes" value="1" checked></div>
                        <div class="radio"><span>N</span> <br> <input type="radio" name="ship" id="shipno" value="0"></div>
                    </div>

                    <div class="input_area">
                        <label for="">Tell us about yourself</label>
                        <textarea name="message" id="message"></textarea>
                    </div>
                    <button type="button" id="sellerApply" value="submit">apply</button>
                </form>
            </div>
        </div>
    </section>




