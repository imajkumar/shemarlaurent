 <div class="col-md-9">
        <!-- my account -->
        <div class="my_account">
            <!-- account information -->
            <div class="account_info general_settings">
                <?php 
                    $genInfo =$this->genInfo;  
                    foreach ($genInfo  as $key => $row) {                        
                        $store_name = $row->store_name;                 
                        $store_email = $row->store_email;                 
                        $business_name = $row->business_name;                 
                        $phone = $row->phone;                 
                        $address = $row->address;                 
                        $apartment_sites = $row->apartment_sites;                 
                        $city = $row->city;                 
                        $country = $row->country;                 
                        $state = $row->state;                 
                        $zip = $row->zip;                 
                        $phone_no = $row->phone_no;     
                    }
                ?>  
                <form action="#">
                    <div class="row">
                        <div class="col-md-4">
                            <!-- content -->
                            <div class="content">
                                <h5>Store Details</h5>
                                <p>The details are to help us contact you incease of an emergency</p>
                            </div>
                            <!-- end content -->
                        </div>
                        <div class="col-md-8">
                            <!-- content -->
                            <div class="content cont7">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Store Name</label>
                                            <input type="text" name="store_name" id="store_name" value = "<?php if(isset($store_name) && !empty($store_name)) { echo $store_name; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Store Email</label>
                                            <input type="email" name="store_email" id="store_email" value = "<?php if(isset($store_email) && !empty($store_email)) { echo $store_email; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                                <h6>This informatin will be saved for a serious emergencies</h6>
                            </div>
                            <!-- end content -->
                        </div>
                        <div class="col-md-4">
                            <!-- content -->
                            <div class="content">
                                <h5>Store Address</h5>
                                <p>The details are to help us contact you incease of an emergency</p>
                            </div>
                            <!-- end content -->
                        </div>
                        <div class="col-md-8">
                            <!-- content -->
                            <div class="content cont7" style="margin-bottom: 0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Legal name of business</label>
                                            <input type="text" name="legal_name" id="legal_name" value = "<?php if(isset($business_name) && !empty($business_name)) { echo $business_name; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">

                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Phone No</label>
                                            <input type="tel" name="phone" id="phone" value = "<?php if(isset($phone) && !empty($phone)) { echo $phone; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Address</label>
                                            <input type="text" name="address" id="address" value = "<?php if(isset($address) && !empty($address)) { echo $address; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Apartment, site etc</label>
                                            <input type="text" name="apartment" id="apartment" value = "<?php if(isset($apartment_sites) && !empty($apartment_sites)) { echo $apartment_sites; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">City</label>
                                            <input type="text" name="city" id="city" value = "<?php if(isset($city) && !empty($city)) { echo $city; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Country</label>
                                            <input type="text" name="country" id="country" value = "<?php if(isset($country) && !empty($country)) { echo $country; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">State</label>
                                            <input type="text" name="state" id="state" value = "<?php if(isset($state) && !empty($state)) { echo $state; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Zip Code</label>
                                            <input type="text" name="zip" id="zip" value = "<?php if(isset($zip) && !empty($zip)) { echo $zip; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-12">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Phone No</label>
                                            <input type="tel" name="phone_no" id="phone_no" value = "<?php if(isset($phone_no) && !empty($phone_no)) { echo $phone_no; } ?>">
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                        </div>
                    </div>
                    <div class="button">
                        
                        <button type="button" id="saveSettings" name="saveSettings">Save</button>
                    </div>
                </form>
            </div>
            <!-- end account information -->
        </div>
        <!-- end my account -->
    </div>