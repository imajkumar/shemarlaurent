<div class="col-md-9">
    <!-- affaliate settings -->
    <div class="affaliate_settings">
        <a href="<?php echo Config::get('BASE_URL') .'/seller/sellersettings'; ?>"><span><i class="fas fa-long-arrow-alt-left"></i></span> back</a>
        <div class="row">
            <div class="col-md-5">
                <!-- text -->
                <div class="text">
                    <h6>Payment Option</h6>
                    <p>The card you enter will be the card you will recive your payments on</p>
                </div>
                <!-- end text -->
            </div>
            <div class="col-md-7">
                <?php 
                    $general =$this->general;  
                    foreach ($general  as $key => $row) {
                        $routing_no = $row->routing_no;
                        $account_no = $row->account_no;
                    }
                ?>
                <form action="#">
                    <div class="content">
                        <a href="#" class="change_account">Change Bank Account</a>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <label for="name">Routing Number</label>
                                    <input type="text" name="routing_no" id="routing_no"  value = "<?php if(isset($routing_no) && !empty($routing_no)) { echo $routing_no; } ?>"  required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <label for="email">Account Number</label>
                                    <input type="email" name="account_no" id="account_no" value = "<?php if(isset($account_no) && !empty($account_no)) { echo $account_no; } ?>"  required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button"><button type="button" id="paymentSetting" name="paymentSetting" >Save</button></div>
                </form>
                <!-- end form -->
            </div>
        </div>
    </div>
    <!-- end affaliate settings -->
</div>