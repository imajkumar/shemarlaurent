    <!--- header area -->
    <section class="header">
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?=Config::get('URL'); ?>">back <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->


    <!-- register -->
    <section class="register reg3">
        <div class="container text-center">
            <div class="content">
                <h6>You're just couple steps closer away from having hyped items in your closet...</h6>
                <div class="bar101">
                    <img src="<?=Config::get('URL'); ?>front/images/5.PNG" alt="" class="img1">
                    <div class="bar">
                        <div class="overlay"></div>
                    </div>
                    <img src="<?=Config::get('URL'); ?>front/images/6.PNG" alt="" class="img2">
                </div>
                <form action="#">
                    <div class="box">
                        <div class="check">
                            <img src="<?=Config::get('URL'); ?>front/images/8.PNG" alt="">
                            <h6>STEP 3 OF 3</h6>
                            <h3>Create your account.</h3>
                            <p>Netflix is personalized for you. Use your email and create a password to watch Netflix on any device at any time.</p>
                        </div>
                    </div>
                    <a href="<?=Config::get('URL'); ?>createAccount">continue</a>
                </form>
            </div>
        </div>
    </section>
    <!-- end register -->


