
<div class="col-md-9">

<a href="<?=Config::get('URL'); ?>seller/addProducts">Add Products</a>
                    <!-- overview -->
                    <div class="overview-content">
                        <!-- search bar -->
                        <div class="search_bar" style="height: auto;">
                            <h3>Products</h3>
                            <div class="row no-gutters">
                                <div class="col-lg-8 col-md-6">
                                    <div class="search">
                                        <form action="#">
                                            <input type="search" name="search">
                                            <button type="submit">search</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="buttons">
                                        <button type="button"><a href="seller-add-product.html">upload</a></button>
                                        <button type="button" class="delete">delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end search bar -->
                    </div>
                    <!-- end overview -->

                    <!-- affaliate grid -->
                    <div class="affaliate_grid admin-orders seller-products">
                        <div class="row">
                        <?php 
                            
                             foreach ($this->products as $key => $row) {
                                
                                ?>
                                <div class="col-lg-3 col-md-4 col-sm-4">
                                <!-- content -->
                                <a href="#" style="color: #000;">
                                    <div class="content">
                                        <div class="box"></div>
                                        <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                        <h3><?=$row->product_title?></h3>
                                    </div>
                                </a>
                                <div class="del10">
                                    <div class="box">
                                        <span><i class="fas fa-times"></i></span>
                                    </div>
                                    <img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt="">
                                </div>
                                <!-- end content -->
                            </div>

                                <?php
                             }
                        ?>

                          

                        </div>
                    </div>
                    <!-- end affaliate grid -->
                </div>
            </div>
        </div>
    </section>
    <!-- end main  content -->






    <!-- confirm delete -->
    <div class="confirm_delet">
        <div class="overlay"></div>
        <div class="content">
            <h6>Are you sure you want to delete this?</h6>
            <button>No, Cencel</button>
            <button>Yes, I am sure</button>
        </div>
    </div>
    <!-- end confirm delete -->



    <!-- live popup -->
    <div class="live_popup">
        <div class="overlay"></div>
        <div class="content">
            <h6>Sorry when you live you can not make any changes.</h6>
            <button>Ok</button>
        </div>
    </div>
    <!-- end live popup -->

