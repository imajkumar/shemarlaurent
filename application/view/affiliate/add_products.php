<div class="col-md-9">
                    <!-- seller add product -->
                    <div class="seller_add_product">
                        <h4>add product</h4>
                        <!-- form -->
                        <form action="<?=Config::get('BASE_URL')?>/seller/SaveProducts" data-redirect="<?=Config::get('URL')?>/seller/addProducts">

                            <!-- content -->
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Brand Name</label>
                                            <input type="text" name="brand_name" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Product Name</label>
                                            <input type="text" name="product_name" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                            <!-- content -->
                            <div class="content">
                                <h6>images <a href="#">Upload Images</a></h6>
                                <div class="upload">
                                    <img src="images/13.PNG" alt="">
                                    <span>Drag &amp; Drop files to upload.</span>
                                </div>
                            </div>
                            <!-- end content -->
                            <!-- content -->
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Price</label>
                                            <input type="text" name="price" readonly required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Country</label>
                                            <div class="select">
                                                <select name="country">
                                                    <option value="select" selected>Select Contry</option>
                                                    <option value="1">USA</option>
                                                    <option value="2">Canada</option>
                                                    <option value="3">England</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                            <!-- content -->
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">My Price</label>
                                            <input type="text" name="seller_price" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Catagory</label>
                                            <div class="select">
                                                <select name="category">
                                                    <option value="1">Volvo</option>
                                                    <option value="2">Saab</option>
                                                    <option value="3">Mercedes</option>
                                                    <option value="4">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                            <!-- content -->
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Weight</label>
                                            <input type="number" name="weight" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Size</label>
                                            <div class="select">
                                                <select name="size">
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                            <!-- content -->
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">SKU</label>
                                            <input type="text" name="sku" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                    <div class="col-md-6">
                                        <!-- input field -->
                                        <div class="input-field">
                                            <label for="">Barcode</label>
                                            <input type="text" name="barcode" required>
                                        </div>
                                        <!-- end input field -->
                                    </div>
                                </div>
                            </div>
                            <!-- end content -->
                            <div class="buttons">
                                <button type="reset">Cancel</button>
                                <button type="button" class="btnActionPost" style="background: #ff00c7; border: 1px solid #ff00c7; color: #fff;">save product</button>
                            </div>
                        </form>
                    </div>
                    <!-- end seller add product -->
                </div>
            </div>
        </div>
    </section>
    <!-- end main  content -->


