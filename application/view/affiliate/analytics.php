<div class="col-md-9">
    <!-- affaliate analytics -->
    <div class="affaliate_analytics">
        <h3>Analytics Overview</h3>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <!-- content -->
                <div class="content">
                    <h5>total hits</h5>
                    <h4>$12000 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <p>Order History</p>
                    <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo1"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #ff6c40;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- content -->
                <div class="content">
                    <h5>total refferals</h5>
                    <h4>400 <span class="up down"><i class="fas fa-long-arrow-alt-down"></i></span> <span class="value down">92%</span></h4>
                        <p>referral history</p>
                        <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo1"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #ff6c40;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- content -->
                <div class="content">
                    <h5>average refferal value</h5>
                    <h4>$2.50 <span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> <span class="value">92%</span></h4>
                    <p>referral history</p>
                        <div class="row no-gutters">
                        <div class="col-1">
                            <ul class="chart_line">
                                <li>100</li>
                                <li>70</li>
                                <li>50</li>
                                <li>30</li>
                                <li>10</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <div class="col-11">
                            <div class="blueberryChart demo1"></div>
                        </div>
                    </div>
                    <ul class="date_bar">
                        <li>January</li>
                        <li>April</li>
                        <li>August</li>
                        <li>December</li>
                    </ul>
                    <div class="text-right">
                        <ul class="name_bar">
                            <li><span style="background: #ff6c40;"></span> You</li>
                            <li><span style="background: #d8d8d8;"></span> Other affaliate</li>
                        </ul>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- content -->
                <div class="content">
                    <h5>top social aveniues</h5>
                    <table>
                        <tr>
                            <td class="brnad">Facebook</td>
                            <td>
                                <h6>450</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Instagram</td>
                            <td>
                                <h6>340</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Youtube</td>
                            <td>
                                <h6>200</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Reddit</td>
                            <td>
                                <h6>120</h6>
                            </td>
                            <td>
                                <p><span class="up down"><i class="fas fa-long-arrow-alt-down"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Pinterest</td>
                            <td>
                                <h6>720</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end content -->
            </div>
            <div class="col-lg-4 col-md-6">
                <!-- content -->
                <div class="content">
                    <h5>wordlwide usage</h5>
                    <table>
                        <tr>
                            <td class="brnad">USA</td>
                            <td>
                                <h6>450</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Canada</td>
                            <td>
                                <h6>340</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Europe</td>
                            <td>
                                <h6>200</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Mexico</td>
                            <td>
                                <h6>120</h6>
                            </td>
                            <td>
                                <p><span class="up down"><i class="fas fa-long-arrow-alt-down"></i></span> 18%</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="brnad">Japan</td>
                            <td>
                                <h6>720</h6>
                            </td>
                            <td>
                                <p><span class="up"><i class="fas fa-long-arrow-alt-up"></i></span> 18%</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- end content -->
            </div>
        </div>
    </div>
    <!-- end affaliate analytics -->

    <!-- footer -->
    <div class="footer">
        <h6>Inquires or request for any addition for analytics dashbord, <a href="affaliate-messengers.html">Click here</a></h6>
    </div>
    <!-- end footer -->
</div>