<div class="col-md-9">
    <!-- affaliate settings -->
    <div class="affaliate_settings">
        <a href="<?php echo Config::get('BASE_URL') .'/affiliate/settings'; ?>"><span><i class="fas fa-long-arrow-alt-left"></i></span> back</a>
        <div class="row">
            <div class="col-md-5">
                <!-- text -->
                <div class="text">
                    <h6>Your Details</h6>
                    <p>This details are to help us contact you encase of an emergancy</p>
                </div>
                <!-- end text -->
            </div>
            <div class="col-md-7">
                <?php
                    $general =$this->general;  
                    foreach ($general  as $key => $row) {
                        $store_name = $row->store_name;
                        $store_email = $row->email;
                    }
                ?>
                <form action="#">
                    <div class="content">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <label for="store_name">Store Name</label>
                                    <input type="text" name="store_name" id="store_name" value = "<?php if(isset($store_name) && !empty($store_name)) { echo $store_name; } ?>" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="input-field">
                                    <label for="email">Store Email</label>
                                    <input type="email" name="email" id="email" value="<?php if(isset($store_email) && !empty($store_email)) { echo $store_email; } ?>" required>
                                </div>
                            </div>
                        </div>
                        <p>This information will be saved for a serious emergencies</p>
                    </div>
                    <div class="button"><button type="button" id="saveSetting" name="saveSetting">Save</button></div>
                </form>
                <!-- end form -->
            </div>
        </div>
    </div>
    <!-- end affaliate settings -->
</div>