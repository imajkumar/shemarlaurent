
<div class="col-md-9">
    <!-- admin settings -->
    <div class="settings admin_settings">
        <h3>Settings</h3>
        <div class="row">
            <div class="col-sm-6">
                <!-- content -->
                <div class="content">
                    <span><i class="fas fa-globe"></i></span>
                    <div class="text">
                        <a href="general">
                            <h5>general</h5>
                            <p>View and update your details</p>
                        </a>
                    </div>
                </div>
                <!-- end content -->
            </div>
          
            <div class="col-sm-6">
                <!-- content -->
                <div class="content">
                    <span><i class="fas fa-credit-card"></i></span>
                    <div class="text">
                        <a href="payment">  
                            <h5>Payment</h5>
                            <p>Update your payment information</p>
                        </a>
                    </div>
                </div>
                <!-- end content -->
            </div>
            <div class="col-sm-6">
                <!-- content -->
                <div class="content">
                    <span><i class="fas fa-university"></i></span>
                    <div class="text">
                        <a href="legal">  
                            <h5>legal</h5>
                            <p>View and update your details</p>
                        </a>
                    </div>
                </div>
                <!-- end content -->
            </div>
        </div>
    </div>
    <!-- end admin settings -->
</div>