<div class="col-md-9">
    <!-- my account -->
    <div class="my_account">
        <!-- verify -->
        <div class="verify">
            <span><i class="fas fa-info-circle"></i></span>
            <div class="text">
                <h5>Verify your email address</h5>
                <p>To keep your account secure, please verify your email address. Send a verification email to "example@gmail.com" or update your email address</p>
                <button type="button">send verification email</button>
            </div>
        </div>
        <!-- end verify -->
        <?php
            $user_data = $this->user_data;                   

            foreach ($user_data as $key => $row) {
                $firstname = $row->firstname;
                $lastname = $row->lastname;
                $user_phone = $row->user_phone;
                $user_email = $row->user_email;               
                
            }
        ?>
        <!-- account information -->
        <div class="account_info">
            <form action="#">
                <div class="row">
                    <div class="col-md-4">
                        <!-- content -->
                        <div class="content">
                            <h5>Account Information</h5>
                        </div>
                        <!-- end content -->
                    </div>
                    <div class="col-md-8">
                        <!-- content -->
                        <div class="profile_pic">
                            <ul>
                                <li><img src="<?=Config::get('URL'); ?>backend/images/1.jpg" alt=""></li>
                                <li><button>change image</button></li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <!-- input field -->
                                <div class="input-field">
                                    <label for="">First Name</label>
                                    <input type="text" name="first_name" id="first_name" value = "<?php echo $firstname; ?>">
                                </div>
                                <!-- end input field -->
                            </div>
                            <div class="col-md-6">
                                <!-- input field -->
                                <div class="input-field">
                                    <label for="">Last Name</label>
                                    <input type="text" name="last_name" id="last_name" value = "<?php echo $lastname; ?>">
                                </div>
                                <!-- end input field -->
                            </div>
                            <div class="col-md-6">
                                <!-- input field -->
                                <div class="input-field">
                                    <label for="#">Email</label>
                                    <input type="email" value="<?php echo $user_email; ?>" name="email" id="email" disabled> <span class="hyper" id="enableEdit">change email</span>
                                </div>
                                <!-- end input field -->
                            </div>
                            <div class="col-md-6">
                                <!-- input field -->
                                <div class="input-field">
                                    <label for="">Phone</label>
                                    <input type="text" name="phone" id="phone" value ="<?php echo $user_phone; ?>">
                                </div>
                                <!-- end input field -->
                            </div>
                        </div>
                        <!-- end content -->
                    </div>
                    <div class="col-md-4">
                        <!-- content -->
                        <div class="content">
                            <h5>Password</h5>
                        </div>
                        <!-- end content -->
                    </div>
                    <div class="col-md-8">
                        <!-- content -->
                        <div class="content">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="">Current Password</label>
                                        <input type="password" name="current_password" id="current_password">
                                    </div>
                                    <!-- end input field -->
                                </div>
                                <div class="col-md-6">
                                    <!-- input field -->
                                    <div class="input-field">

                                    </div>
                                    <!-- end input field -->
                                </div>
                                <div class="col-md-6 d-none d-lg-block d-md-block">
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="">New Password</label>
                                        <input type="password" name="new_password" id="new_password">
                                    </div>
                                    <!-- end input field -->
                                </div>
                                <div class="col-md-6">
                                    <!-- input field -->
                                    <div class="input-field">
                                        <label for="">Re_type Password</label>
                                        <input type="password" name="re_password" id="re_password">
                                    </div>
                                    <!-- end input field -->
                                </div>
                            </div>
                        </div>
                        <!-- end content -->
                    </div>
                </div>
                <div class="button">
                    <button type="button" name="update_profile" id="update_profile">Save</button>
                </div>
            </form>
        </div>
        <!-- end account information -->
    </div>
    <!-- end my account -->
</div>