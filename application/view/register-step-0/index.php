 <!--- header area -->
 <section class="header">
        <div class="container-fluid">
            <!-- main nav -->
            <div class="main-nav">
                <nav class="navbar navbar-expand-lg">
                    <div class="logo">
                        <a class="navbar-brand" href="<?=Config::get('URL'); ?>">
                            <img src="<?=Config::get('URL'); ?>front/images/logo.PNG" alt="logo">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?=Config::get('URL'); ?>">back <span class="sr-only">(current)</span></a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- end main nav -->
        </div>
    </section>
    <!--- end header area -->
    
    
    <!-- register -->
    <section class="register">
        <div class="container text-center">
            <div class="content">
                <h6>You're just couple steps closer away from having hyped items in your closet...</h6>
                <div class="bar101">
                    <img src="images/5.PNG" alt="" class="img1">
                    <div class="bar"><div class="overlay"></div></div>
                    <img src="images/6.PNG" alt="" class="img2">
                </div>
                <form action="#">
                    <div class="box">
                        <div class="check">
                            <span><i class="far fa-check-circle"></i></span>
                            <h6>STEP 1 OF 3</h6>
                            <h3>Choose your plan.</h3>
                        </div>
                        <ul>
                            <li><span><i class="fas fa-check"></i></span> You won't be charged until after your free month.</li>
                            <li><span><i class="fas fa-check"></i></span> We'll remind you three days before your trial ends.</li>
                            <li><span><i class="fas fa-check"></i></span> No commitments, cancel anytime.</li>
                        </ul>
                        <a href="<?=Config::get('URL'); ?>registrationstep2">see the plans</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- end register -->
    
    