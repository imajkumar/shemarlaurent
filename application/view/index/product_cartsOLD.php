<!-- cart area -->
<section class="cart_area">
        <div class="container">
        <form action="<?=Config::get('URL'); ?>index/paynow" method="post">
        <input type="submit" value="Send">
        </form>
            <div class="row">
                <div class="col-md-6">
                    <!-- content -->
                    <div class="content">
                        <h4>Checkout</h4>

                        <!-- box -->
                        <div class="box">
                            <h5>Payment Method</h5>
                            <div class="cont">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="name">Cardholder Name</label>
                                            <input type="text" name="name" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-9">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="number">Card Number</label>
                                            <input type="text" name="number" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="date">Expire Date</label>
                                            <input type="text" name="date" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">Security Code</label>
                                            <input type="text" name="code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">Zip Code</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end box -->


                        <!-- box -->
                        <div class="box">
                            <h5>Shipping Information</h5>
                            <div class="cont">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="number">Shipping Address</label>
                                            <input type="text" name="number" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="date">Apt/Suite</label>
                                            <input type="text" name="date" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">City</label>
                                            <input type="text" name="code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">State/Region</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">Country</label>
                                            <div class="select">
                                                <span><i class="fas fa-angle-down"></i></span>
                                                <select>
                                                    <option value="volvo" selected>USA</option>
                                                    <option value="saab">Bangladesh</option>
                                                    <option value="vw">Australia</option>
                                                    <option value="audi">China</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">Zip Code</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end box -->
                    </div>
                    <!-- end content -->

                    <!-- content -->
                    <div class="content" style="margin-top: -45px;">
                        <h4>Cart</h4>
                        <div class="box">
                            <div class="cont">
                                <div class="row">
                                <?php
                                if(isset($_SESSION["cart_item"])){
                                    $total_quantity = 0;
                                    $total_price = 0;
                                ?>	
                                <?php		
                                foreach ($_SESSION["cart_item"] as $item){
                                    $item_price = $item["quantity"]*$item["price"];
                                    ?>

                                    <div class="col-md-4">
                                        <div class="image">
                                            <img src="<?php echo $item["image"]; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <p><?php echo $item["name"]; ?> <span><?php echo "$ ".$item["price"]; ?></span></p>
                                        <p>QTY:<?php echo $item["quantity"]; ?></p>
                                        <button  class="removeItem" type="button" id="<?php echo $item["code"]; ?>">Remove</button>
                                    </div>

                                           
                                            <?php
                                            $total_quantity += $item["quantity"];
                                            $total_price += ($item["price"]*$item["quantity"]);
                                    }
                                   }else{
                                       echo "<b>Empty Basket</b>";
                                   }



                                    ?>


                                   

                                </div>
                            </div>
                        </div>
                        <h6>Want to buy more items but your almost out? <a href="#">Add a BOOST!</a></h6>
                    </div>
                    <!-- end content -->
                </div>
                <div class="col-md-6">
                    <!-- content -->
                    <div class="content">
                        <div class="box b11">
                           <h5>Order Summary</h5>
                            <div class="cont">
                            <?php
                            $total_price=0;
                                if(isset($_SESSION["cart_item"])){
                                    $total_price += ($item["price"]*$item["quantity"]);

                                }
                        ?>

                                <ul>
                                    <li>Subtotal</li>
                                    <li>Shipping & Handling</li>
                                    <li>Bost</li>
                                </ul>
                                <hr>
                                <h6>Order Total : <?="$".$total_price?></h6>
                                <p>By clicking below, I agree the terms of use and refund policy and that I have read the privacy statement.</p>
                                <button type="submit"><a href="thank-you.html">order now</a></button>
                            </div>
                        </div>
                    </div>
                    <!-- end content -->
                </div>
            </div>
        </div>
    </section>
    <!-- end cart area -->
    