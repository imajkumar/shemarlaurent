<!-- banner area -->
    <section class="banner">
        <div class="container">
            <h5>The Stock Market of Things®</h5>
            <h1>Buy and Sell Authentic sneakers</h1>
        </div>
    </section>
    <!-- end banner area -->

    <div id="load_data" style="display:none">

   
    </div>
   <div id="load_data_message"></div>


    <!-- product area -->
    <section class="product-area new-items">
        <div class="container">
           <div class="top-bar">
              <h2>new items</h2>
               <img src="https://i.postimg.cc/Z506pmzd/download.png" alt="">
           </div>
          
            <div class="row">
            <?php
                
                $products=$this->products;
               
                
                if(count($products)>0){
                    foreach ($products as $key => $rowData) {
                        
                        if(!empty($rowData->cover_img)){
                            $imgURl=Config::get('URL').$rowData->cover_img;
                            
                          }else{
                              $imgURl=Config::get('URL')."front/images/cross.png";
                          }



                       ?>
                       <div class="col-lg-3 col-md-4 col-sm-sm-6 col-6">
                            <!-- content -->
                            <div class="content">
                                <div class="product-image">
                                <img src="<?=$imgURl?>" alt="<?=$rowData->product_title?>">

                                    <span><?=$rowData->seller_price?></span>
                                </div>
                                <h5><a href="<?=Config::get('URL'); ?>product/view/<?=$rowData->id?>"><?=$rowData->product_title?></a></h5>
                            </div>
                            <!-- end content -->
                        </div>
                       <?php
                    }
                    
                }

                
            ?>

              
               
                
            </div>
        </div>
        <!-- icon bar -->
        <div class="icon_bar">
            <div class="overlay"></div>
            <div class="icons">
                <ul>
                    <li><a href="<?=Config::get('URL');?>product/carts">Cart</a></li>
                    <li class="search_icon"><Span>Search</Span></li>
                    <li class="expendli">Expend</li>
                </ul>
            </div>
        </div>
        <!-- end icon bar -->
    </section>
    <!-- end product area -->


    



    <!-- search area -->
    <section class="search">
       <div class="overlay"></div>
        <div class="box">
            <div class="searchx">
                <span>Search</span>
                <input type="text" name="search" id="txtSearch">
                <div class="button"><button type="button"><img src="<?=Config::get('URL'); ?>front/images/search.png" alt=""></button></div>
            </div>
        </div>
    </section>
    <!-- end search area -->

    <!-- expend area -->
    <section class="expend">
        <div class="container-fluid">
            <div class="content">
                <img src="<?=Config::get('URL'); ?>front/images/cross.png" alt="">
                <ul>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- end expend area -->
