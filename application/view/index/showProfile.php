 <!-- my account -->

 
 <section class="my-account">
        <div class="container">
            <h4>My account <a href="<?php echo Config::get('URL'); ?>login/logout">Log out</a></h4>
            <div class="account">
                <!-- verification -->
                <?php 
                $data=$this->user;
                ?>

                <div class="verification">

                    <img src="<?=Config::get('URL'); ?>front/images/1.PNG" alt="">
                    <h6>Verify your email address</h6>
                    <p>To help keep your account secure, please verify your email address. Send a verification email to "<?=$data->user_email;?>" or update your email address.</p>
                    <button type="button" id="btnSendEmailVerification" >Send verification email</button>
                </div>
                <!-- end verification -->

                <!-- tickets -->
                <div class="tickets">
                    <h4>Tickets <span><i class="fas fa-exclamation"></i></span></h4>
                    <div class="loading_bar">
                        <img src="<?=Config::get('URL'); ?>front/images/2.PNG" alt="" class="img1">
                        <div class="bar">
                            <div class="color"></div>
                        </div>
                        <img src="<?=Config::get('URL'); ?>front/images/3.PNG" alt="" class="img2">
                        <h5>level 1 <span>level 2</span></h5>
                    </div>
                </div>
                <!-- end tickets -->
                <!-- membership -->
                <div class="membership">
                    <h4>Membership <br> <span>Account</span></h4>
                    <button type="button">Cancel Membership</button>
                    <form action="#">
                        <!-- account -->
                        <div class="account">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="input_area">
                                        <label for="">First Name</label>
                                        <input type="text" id="firstname" value="<?=$data->firstname;?>" required >
                                    </div>
                                    <div class="input_area">
                                        <label for="">Last Name</label>
                                        <input type="text" id="lastname" value="<?=$data->lastname;?>"   required>
                                    </div>
                                    <div class="input_area email_view">
                                        <label for="">Email</label>
                                        <input type="text" id="email" class="emailView" value="<?=$data->user_email;?>"   required>
                                    </div>

                                </div>
                                <div class="col-md-8">
                                    <div class="primary">
                                        <div class="top">
                                            <h5>Visa ending in 1234- 9/2023 <span>Primary</span></h5>
                                            <img src="<?=Config::get('URL'); ?>front/images/cross.png" alt="">
                                        </div>
                                        <ul>
                                            <li>Memvership Expires: July 24, 2019 (7/24/19)</li>
                                            <li>Account Expire: July 30, 2019, (8/02/20) <a href="#">Change payment option</a></li>
                                        </ul>
                                       

                                        <h6><?=$data->user_email;?> <a href="javascript::void(0)" id="btnChangeEmail">Change email</a></h6>
                                        <h6> <a href="javascript::void(0)" id="btnChangePassword">Change Password</a></h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end account -->
                        <hr>
                        <!-- password -->
                        <div class="password">
                            <h6>Password</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input_area">
                                        <label for="">Old Password</label>
                                        <input  class="passwordview" type="password" id="oldPassword" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input_area i303" style="opacity: 0;">
                                        <label for="">New Password</label>
                                        <input type="password" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input_area">
                                        <label for="">New Password</label>
                                        <input type="password"  class="passwordview"  id="newPassword" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input_area">
                                        <div class="f-right">
                                            <label for="">Re-type Password</label>
                                            <input type="password"  class="passwordview"  id="reTypePassword" required>
                                        </div>
                                    </div>
                                   
                                </div>
                                
                            </div>
                        </div>

                        <!-- end password -->
                       <input type="hidden" id="user_id" value="<?=$data->user_id?>">
                        <!-- address -->
                        <div class="address">
                            <h6>Address</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <textarea name="user_address" id="user_address" readonly><?=$data->address;?></textarea>
                                </div>
                                <div class="col-md-6">
                                    <div class="text">
                                    <a href="javascript::void(0)" id="btnChanageAddress">Change Address</a></div>
                                </div>
                            </div>
                        </div>
                        <!-- end address -->
                        <div class="save">
                            <button type="button" id="btnUpdateUserProfile">save</button>
                        </div>
                    </form>
                </div>
                <!-- end membership -->
            </div>
        </div>
    </section>
    <!-- end my account -->