<!-- cart area -->
<section class="cart_area">
        <div class="container">
        <!-- <form action="<?=Config::get('URL'); ?>index/paynow" method="post">
        <input type="submit" value="Send">
        </form> -->
        <form action="<?=Config::get('URL'); ?>index/paynow" method="post">

            <div class="row">
                <div class="col-md-6">
                    <!-- content -->
                    <div class="content">
                        <h4>Checkout</h4>

                        <!-- box -->
                        <div class="box">
                            <h5>Payment Method</h5>
                            <div class="cont">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="name">Cardholder Name</label>
                                            <input type="text" name="card_holder_name" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-9">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="number">Card Number</label>
                                            <input type="text" name="card_number" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="date">Expire Date</label>
                                            <input type="text" name="card_exp" placeholde="eg: 12/122" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">Security Code</label>
                                            <input type="text" name="code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">Zip Code</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end box -->


                        <!-- box -->
                        <div class="box">
                            <h5>Shipping Information</h5>
                            <div class="cont">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="number">Shipping Address</label>
                                            <input type="text" name="number" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="date">Apt/Suite</label>
                                            <input type="text" name="date" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">City</label>
                                            <input type="text" name="code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">State/Region</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-8">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="code">Country</label>
                                            <div class="select">
                                                <span><i class="fas fa-angle-down"></i></span>
                                                <select>
                                                    <option value="volvo" selected>USA</option>
                                                    <option value="saab">Bangladesh</option>
                                                    <option value="vw">Australia</option>
                                                    <option value="audi">China</option>
                                                </select>
                                            </div>
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                    <div class="col-md-4">
                                        <!-- input area -->
                                        <div class="input_area">
                                            <label for="zip_code">Zip Code</label>
                                            <input type="text" name="zip_code" required class="d-block">
                                        </div>
                                        <!-- end input area -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end box -->
                    </div>
                    <!-- end content -->

                   
                </div>
                <div class="col-md-6">
                    <!-- content -->
                    <div class="content">
                        <div class="box b11">
                           <h5>Order Summary</h5>
                            <div class="cont">
                            <?php
                            $total_price=0;
                                if(isset($_SESSION["cart_item"])){
                                    $total_price =111;

                                }
                        ?>

                                <ul>
                                    <li>Subtotal</li>
                                    <li>Shipping & Handling</li>
                                    <li>Bost</li>
                                </ul>
                                <hr>
                                <h6>Order Total : <?="$".$total_price?></h6>
                               
                                <!-- <button type="submit"><a href="#">Procced to Payment</a></button> -->
                                <button type="submit">Procced to Payment</button>
                            </div>
                            </form>
                        </div>
                    </div>
                    <!-- end content -->
                </div>
            </div>
        </div>
    </section>
    <!-- end cart area -->
    