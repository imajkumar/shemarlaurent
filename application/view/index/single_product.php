<!-- product name -->
<?php 
$products=$this->products_details;


if(!empty($products->cover_img)){
    $imgURl=Config::get('URL').$products->cover_img;
    
  }else{
      $imgURl=Config::get('URL')."front/images/cross.png";
  }


?>
<section class="product-name">
        <div class="container">
           <div class="imageqq d-none d-lg-block">
               <img src="<?=$imgURl?>" alt="<?=$products->product_title?>">
           </div>
            <div class="content">
                <h5><?=$products->product_title?></h5>
                <div class="product_image">
                    <span><i class="fas fa-plus"></i></span>
                </div>
                <div class="buttons">
                    <button type="button" class="cart_btn" id="<?=$products->id?>"><a href="javascript::void(0)">add to cart</a></button>
                    <button type="button" class="buy_now">buy now</button>
                </div>
            </div>
        </div>
    </section>
    <!-- end product name -->
    
   

    <!-- more item -->
    <section class="more-item">
        <div class="container">
            <div class="content">
               <h4>More Item</h4>
                <ul>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                     <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                    <li><a href="#"></a></li>
                </ul>
            </div>
        </div>
    </section>
    <!-- end more item -->