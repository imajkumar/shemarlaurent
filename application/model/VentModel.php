<?php

/**
 * VentModel
 * Handle Vent Model and specific functionality
 */
class VentModel
{
    public static function addtheVent(){     
       
        $vent_message=Request::post('vent_message');
        
        $database = DatabaseFactory::getFactory()->getConnection();
        // write new users data into database
        $sql = "INSERT INTO vent (vent_message)
                    VALUES (:vent_message)";
        $query = $database->prepare($sql);
        $query->execute(array(':vent_message' => $vent_message,
                              ));
        $count =  $query->rowCount();

        if ($count == 1) {
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('VENT_ADDED_SUCCESS_MESSAGE'),
                'status'=>'1'
            ); 

        }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('VENT_ADDED_SUCCESS_MESSAGE'),
                'status'=>'0'
            ); 
        }

        echo json_encode($data);

    }
    
}