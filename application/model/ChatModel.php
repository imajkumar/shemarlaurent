<?php

/**
 * ChatModel
 * Handle Complete Chat Model and specific functionality
 */
class ChatModel
{
    public static function sendtheChat($for){     
       
        $msg = Request::post('msg');
        if($for == "seller"){
            $sender = Request::post('senderid');
            $adminId = 3; 
        }else if($for == "admin"){
            $sender = 3;
            $adminId = Request::post('senderid'); 
        }
        
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "INSERT INTO chat (sender, receiver, message)
                    VALUES (:sender, :receiver, :msg)";
        $query = $database->prepare($sql);
        $query->execute(array(                             
                                ':sender' => $sender, 
                                ':receiver' => $adminId,
                                ':msg' => $msg,                               
                            ));
        $count =  $query->rowCount();
        if ($count == 1) {
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('CHAT_ADDED_SUCCESS_MESSAGE'),
                'status'=>'1'
            ); 

        }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('CHAT_ADDED_SUCCESS_MESSAGE'),
                'status'=>'0'
            ); 
        }
        echo json_encode($data);

    }
    public static function ChatUsers(){     
       
        $database = DatabaseFactory::getFactory()->getConnection();       
        $sql = "select * from users where user_account_type !=7";
        $query = $database->prepare($sql);
        $query->execute();
        $chatuser = '';
        foreach ($query->fetchAll() as $user) {
         
                $chatuser .= '<div class="owl-item cloned">
                                <div class="item" id="'.$user->user_id.'" onclick="activechat(this.id)">
                                <div class="content">
                                    <div class="box"></div>
                                        <h4>'. $user->firstname . ' '. $user->lastname .'</h4>
                                    </div></div></div>
                                    ';              
            
        }
        $chatuser  .='<script>
        function activechat(id){          
           window.location.href = BASE_URL + "/admin/messanger?chatUser=" + escape( id);
        }
        </script>';
        echo $chatuser;
    }
    public static function fetchtheChat($for){
        $database = DatabaseFactory::getFactory()->getConnection(); 
        
        if($for == 'admin'){
            $adminId = 3;
            $userId = Request::post('id');
            $checkChat ="SELECT count(*) FROM chat where (sender = '".$userId ."' and receiver = '". $adminId ."') or (sender= '".$adminId ."' and receiver='".$userId ."')";
            $query = $database->prepare($checkChat);
            $query->execute();  
            $chatmsg = '';  
            if ($query->fetchColumn() > 1) {  

                $sql = "select * from chat where sender IN ('".$userId."','".$adminId."') and receiver IN('".$adminId."','".$userId."') ORDER BY chat_date ASC";
                $query = $database->prepare($sql);
                $query->execute();

                $all_users_profiles = array();
               
                foreach ($query->fetchAll() as $user) {
                    if($user->receiver !=  $adminId){
                        $chatmsg .= '  <div class="recive">
                        <p>'. $user->message .'</p>
                    </div>';
                    }else{
                        $chatmsg .= '  <div class="send">
                        <p>'. $user->message .'</p>
                    </div>';
                    }     
                    
                }
            }else{
                $chatmsg .= '  <div class="send">
                <p>There is no chat history</p>
            </div>';
            }
        }else if($for == 'seller'){
            $userId = Request::post('id');
            $adminId = 3 ;
            $checkChat ="SELECT count(*) FROM chat where (sender = '".$userId ."' and receiver = '". $adminId ."') or (sender= '".$adminId ."' and receiver='".$userId ."')";
            $query = $database->prepare($checkChat);
            $query->execute();  
            $chatmsg = '';  
            if ($query->fetchColumn() > 1) {  
                    
                $sql = "select * from chat where sender IN ('".$userId."','".$adminId."') and receiver IN('".$adminId."','".$userId."') ORDER BY chat_date ASC";
                $query = $database->prepare($sql);
                $query->execute();

                $all_users_profiles = array();
                $chatmsg = '';
                foreach ($query->fetchAll() as $user) {
                    if($user->receiver !=  $userId){
                        $chatmsg .= '  <div class="recive">
                        <p>'. $user->message .'</p>
                    </div>';
                    }else{
                        $chatmsg .= '  <div class="send">
                        <p>'. $user->message .'</p>
                    </div>';
                    }     
                    
                }
            }else{
                $chatmsg .= '  <div class="send">
                    <p>There is no chat history</p>
                </div>';
            }
        }
        echo $chatmsg;
    }
    
}