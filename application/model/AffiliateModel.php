<?php

/**
 * SellerModel
 * Handle Complete Seller Model and specific functionality
 */
class AffiliateModel
{
    //Save General Setting
    public static function getGeneral(){
        $affiliate_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT * FROM affiliate_gen_settings WHERE affiliate_id = :affiliate_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':affiliate_id' => $affiliate_id));      
        $general = array();
        foreach ($query->fetchAll() as $set) {            
            $general[$set->id] = new stdClass();
            $general[$set->id]->id = $set->id;
            $general[$set->id]->store_name = $set->store_name;                 
            $general[$set->id]->email = $set->email;                 
        }

        return $general;
    }
    public static function saveAffSettings(){     
        $store_name=Request::post('store_name');
        $email=Request::post('email');      
        $affiliate_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT COUNT(*) FROM affiliate_gen_settings WHERE affiliate_id = :affiliate_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':affiliate_id' => $affiliate_id));
        
        if ( $query->fetchColumn() < 1) {            
            $sql = "INSERT INTO affiliate_gen_settings (affiliate_id, store_name, email)
                        VALUES (:affiliate_id, :store_name, :email)";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':affiliate_id' => $affiliate_id,                         
                                    ':store_name' => $store_name,
                                    ':email' => $email   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'1'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'0'
                ); 
            }
        }else{
            $sql = "UPDATE affiliate_gen_settings SET store_name=:store_name, email=:email WHERE affiliate_id=:affiliate_id";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':affiliate_id' => $affiliate_id,                         
                                    ':store_name' => $store_name,
                                    ':email' => $email   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'3'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }
        echo json_encode($data);
    }
  
    public static function getPayment(){
        $affiliate_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT * FROM affiliate_payment_settings WHERE affiliate_id = :affiliate_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':affiliate_id' => $affiliate_id));      
        $general = array();
        foreach ($query->fetchAll() as $set) {            
            $general[$set->id] = new stdClass();
            $general[$set->id]->id = $set->id;
            $general[$set->id]->account_no = $set->account_no;                 
            $general[$set->id]->routing_no = $set->routing_no;                 
        }

        return $general;
    }
    public static function savePaySettings(){   

        $routing_no=Request::post('routing_no');
        $account_no=Request::post('account_no');      
        $affiliate_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT COUNT(*) FROM affiliate_payment_settings WHERE affiliate_id = :affiliate_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':affiliate_id' => $affiliate_id));

        if ($query->fetchColumn() < 1) {            
            $sql = "INSERT INTO affiliate_payment_settings  (affiliate_id, account_no, routing_no)
                        VALUES (:affiliate_id, :account_no, :routing_no)";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':affiliate_id' => $affiliate_id,                         
                                    ':account_no' => $account_no,
                                    ':routing_no' => $routing_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'1'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'0'
                ); 
            }
        }else{
            $sql = "UPDATE affiliate_payment_settings SET account_no=:account_no, routing_no=:routing_no WHERE affiliate_id=:affiliate_id";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':affiliate_id' => $affiliate_id,                         
                                    ':account_no' => $account_no,
                                    ':routing_no' => $routing_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'3'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }
        echo json_encode($data);

    }
}