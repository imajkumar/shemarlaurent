<?php

/**
 * SellerModel
 * Handle Complete Seller Model and specific functionality
 */
class sellerModel
{
    public static function getGen(){
        $seller_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT * FROM seller_settings WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));      
        $genset = array();
        foreach ($query->fetchAll() as $set) {            
            $genset[$set->id] = new stdClass();
            $genset[$set->id]->id = $set->id;
            $genset[$set->id]->store_name = $set->store_name;                 
            $genset[$set->id]->store_email = $set->store_email;                 
            $genset[$set->id]->business_name = $set->business_name;                 
            $genset[$set->id]->phone = $set->phone;                 
            $genset[$set->id]->address = $set->address;                 
            $genset[$set->id]->apartment_sites = $set->apartment_sites;                 
            $genset[$set->id]->city = $set->city;                 
            $genset[$set->id]->country = $set->country;                 
            $genset[$set->id]->state = $set->state;                 
            $genset[$set->id]->zip = $set->zip;                 
            $genset[$set->id]->phone_no = $set->phone_no;                        
        }

        return $genset;
    }
    //Save General Setting 
    public static function saveSellerSettings(){     
        $store_name=Request::post('store_name');
        $store_email=Request::post('store_email');
        $legal_name=Request::post('legal_name');        
        $phone=Request::post('phone');
        $address=Request::post('address');  
        $apartment=Request::post('apartment');  
        $city=Request::post('city');  
        $country=Request::post('country');  
        $state=Request::post('state');  
        $zip=Request::post('zip');  
        $phone_no=Request::post('phone_no');  
        $seller_id= Session::get('user_id');        
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT COUNT(*) FROM seller_settings WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));

        if ($query->fetchColumn() < 1) {   

            $sql = "INSERT INTO seller_settings (seller_id, store_name, store_email, business_name, phone,
            address, apartment_sites, city, country, state, zip, phone_no)
                        VALUES (:seller_id, :store_name, :store_email, :legal_name, :phone, :address, :apartment, 
                        :city, :country, :state, :zip, :phone_no)";
            $query = $database->prepare($sql);
            $query->execute(array(                             
                                    ':seller_id' => $seller_id, 
                                    ':store_name' => $store_name,
                                    ':store_email' => $store_email,
                                    ':legal_name' => $legal_name,
                                    ':phone' => $phone,
                                    ':address' => $address,
                                    ':apartment' => $apartment,
                                    ':city' => $city,
                                    ':country' => $country,
                                    ':state' => $state,
                                    ':zip' => $zip,
                                    ':phone_no' =>$phone_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'1'
                ); 
            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }else{
            $sql = "UPDATE seller_settings SET store_name=:store_name, store_email=:store_email, business_name=:legal_name,
            phone=:phone, address=:address, apartment_sites=:apartment, city=:city, country=:country,
            state=:state, zip=:zip, phone_no=:phone_no WHERE seller_id=:seller_id";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                ':seller_id' => $seller_id, 
                                ':store_name' => $store_name,
                                ':store_email' => $store_email,
                                ':legal_name' => $legal_name,
                                ':phone' => $phone,
                                ':address' => $address,
                                ':apartment' => $apartment,
                                ':city' => $city,
                                ':country' => $country,
                                ':state' => $state,
                                ':zip' => $zip,
                                ':phone_no' =>$phone_no  
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'3'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }
        echo json_encode($data);

    }


    public static function getShip(){
        $seller_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT * FROM seller_shipping_setting WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));      
        $genset = array();
        foreach ($query->fetchAll() as $set) {            
            $genset[$set->id] = new stdClass();
            $genset[$set->id]->id = $set->id;
            $genset[$set->id]->location_name = $set->location_name;
            $genset[$set->id]->address = $set->address;                 
            $genset[$set->id]->apartment_sites = $set->apartment_sites;                 
            $genset[$set->id]->city = $set->city;                 
            $genset[$set->id]->country = $set->country;                 
            $genset[$set->id]->state = $set->state;                 
            $genset[$set->id]->zip = $set->zip;                 
            $genset[$set->id]->phone_no = $set->phone_no;                        
        }

        return $genset;
    }
    public static function saveShipSettings(){     
        $location=Request::post('location');
        $address=Request::post('address');  
        $apartment=Request::post('apartment');  
        $city=Request::post('city');  
        $country=Request::post('country');  
        $state=Request::post('state');  
        $zip=Request::post('zip');  
        $phone_no=Request::post('phone_no');  
        $seller_id= Session::get('user_id');        
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT COUNT(*) FROM seller_shipping_setting WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));

        if ($query->fetchColumn() < 1) {  
            $sql = "INSERT INTO seller_shipping_setting (seller_id, location_name, address, apartment_sites, city, country, state, zip, phone_no)
                        VALUES (:seller_id, :location, :address, :apartment, 
                        :city, :country, :state, :zip, :phone_no)";
            $query = $database->prepare($sql);
            $query->execute(array(                             
                                    ':seller_id' => $seller_id,
                                    ':location' => $location,
                                    ':address' => $address,
                                    ':apartment' => $apartment,
                                    ':city' => $city,
                                    ':country' => $country,
                                    ':state' => $state,
                                    ':zip' => $zip,
                                    ':phone_no' =>$phone_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'1'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'0'
                ); 
            }
        }else{
            // location_name, address, apartment_sites, city, country, state, zip, phone_no
            $sql = "UPDATE seller_shipping_setting SET location_name=:location,
            address=:address, apartment_sites=:apartment, city=:city, country=:country,
            state=:state, zip=:zip, phone_no=:phone_no WHERE seller_id=:seller_id";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                ':seller_id' => $seller_id,
                                ':location' => $location,
                                ':address' => $address,
                                ':apartment' => $apartment,
                                ':city' => $city,
                                ':country' => $country,
                                ':state' => $state,
                                ':zip' => $zip,
                                ':phone_no' =>$phone_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'3'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }
        echo json_encode($data);

    }
    public static function getPayment(){
        $seller_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT * FROM seller_payment_settings WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));      
        $general = array();
        foreach ($query->fetchAll() as $set) {            
            $general[$set->id] = new stdClass();
            $general[$set->id]->id = $set->id;
            $general[$set->id]->account_no = $set->account_no;                 
            $general[$set->id]->routing_no = $set->routing_no;                 
        }

        return $general;
    }
    public static function savePaySettings(){   

        $routing_no=Request::post('routing_no');
        $account_no=Request::post('account_no');      
        $seller_id= Session::get('user_id');  
        $database = DatabaseFactory::getFactory()->getConnection();        
        $sql = "SELECT COUNT(*) FROM seller_payment_settings WHERE seller_id = :seller_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id));

        if ($query->fetchColumn() < 1) {            
            $sql = "INSERT INTO seller_payment_settings (seller_id, account_no, routing_no)
                        VALUES (:seller_id, :account_no, :routing_no)";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':seller_id' => $seller_id,                         
                                    ':account_no' => $account_no,
                                    ':routing_no' => $routing_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'1'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_ADDED_SUCCESS_MESSAGE'),
                    'status'=>'0'
                ); 
            }
        }else{
            $sql = "UPDATE seller_payment_settings SET account_no=:account_no, routing_no=:routing_no WHERE seller_id=:seller_id";
            $query = $database->prepare($sql);
            $query->execute(array(    
                                    ':seller_id' => $seller_id,                         
                                    ':account_no' => $account_no,
                                    ':routing_no' => $routing_no   
                                ));
            $count =  $query->rowCount();
            if ($count == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'3'
                ); 

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('SETTING_UPDATED_SUCCESS_MESSAGE'),
                    'status'=>'4'
                ); 
            }
        }
        echo json_encode($data);

    }
}