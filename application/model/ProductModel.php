<?php

/**
 * UserModel
 * Handles all the PUBLIC profile stuff. This is not for getting data of the logged in user, it's more for handling
 * data of all the other users. Useful for display profile information, creating user lists etc.
 */
class ProductModel
{
    

    public static function updateProductIMG(){
      

        $uploadDir = "uploads/";
        
     if(isset($_FILES['dropImage'])){
            if (is_uploaded_file($_FILES['dropImage']['tmp_name'])) {
                $sourcePath = $_FILES['dropImage']['tmp_name'];
                echo $targetPath =  $_FILES['dropImage']['name'];
                
                if (move_uploaded_file($sourcePath, $targetPath)) {
                    print $targetPath;
                }
            }
        }

    }
    public static function removeFromCart($code){
        if(!empty($_SESSION["cart_item"])) {
            foreach($_SESSION["cart_item"] as $k => $v) {
               
                if($code == $v['code'])
                    unset($_SESSION["cart_item"][$k]);				
                if(empty($_SESSION["cart_item"]))
                    unset($_SESSION["cart_item"]);
            }
        }
         $data=array(
                'label'=>'success',
                'txt'=>Text::get('REMOVE_ITEM_TO_CART_SUCCESS_MESSAGE'),
                'status'=>1,

            );

           echo    json_encode($data);

    }
    public static function addToCart($product_id,$quantity){
   
        $product_arr= Self::getProductWithID($product_id);
     
           // $productByCode = $db_handle->runQuery("SELECT * FROM tblproduct WHERE code='" . $_GET["code"] . "'");
           if(!empty($product_arr->cover_img)){
            $imgURl=Config::get('URL').$product_arr->cover_img;
            
          }else{
              $imgURl=Config::get('URL')."front/images/cross.png";
          }
           $imgURl;
        
            $itemArray = array(
                $product_arr->id=>
                array(
                     'name'=>$product_arr->product_title,
                     'code'=>$product_arr->id, 
                     'quantity'=>$quantity, 
                     'price'=>$product_arr->seller_price,
                     'image'=>$imgURl
                    )
            );
            if(!empty($_SESSION["cart_item"])) {
                if(in_array($product_arr->id,array_keys($_SESSION["cart_item"]))) {
                    foreach($_SESSION["cart_item"] as $k => $v) {
                            if($product_arr->id == $k) {
                                if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $quantity;
                            }
                    }
                } else {
                    $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                }
            } else {
                $_SESSION["cart_item"] = $itemArray;
            }
            
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('ADD_TO_CART_SUCCESS_MESSAGE'),
                'status'=>1,

            );

           echo    json_encode($data);

    }

    public static function getProductWithID($product_id){


        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT * 
                FROM products WHERE id = :id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':id' => $product_id));
        return $query->fetch();

      
      
    }

    public static function getAllProuducts(){

        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT * FROM products";
        $query = $database->prepare($sql);
        $query->execute();

        $all_user_products = array();

        foreach ($query->fetchAll() as $product) {

            // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
            // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
            // the user's values
            array_walk_recursive($product, 'Filter::XSSFilter');

            $all_users_profiles[$product->id] = new stdClass();
            $all_users_profiles[$product->id]->id = $product->id;
            $all_users_profiles[$product->id]->seller_id = $product->seller_id;
            $all_users_profiles[$product->id]->product_title = $product->product_title;
            $all_users_profiles[$product->id]->category = $product->category;

            $getBrandName = "SELECT brand_name FROM brand where id='".$product->brand."'";
            $getquery = $database->prepare($getBrandName);
            $getquery->execute();
            foreach($getquery->fetchAll() as $brand){
                $all_users_profiles[$product->id]->brand = $brand->brand_name;
            }
            
            $all_users_profiles[$product->id]->product_sku = $product->product_sku;
            $all_users_profiles[$product->id]->barcode = $product->barcode;
            $all_users_profiles[$product->id]->sizes = $product->sizes;
            $all_users_profiles[$product->id]->weight = $product->weight;
            $all_users_profiles[$product->id]->seller_price = $product->seller_price;
            $all_users_profiles[$product->id]->admin_price = $product->admin_price;
            $all_users_profiles[$product->id]->country = $product->country;
            $all_users_profiles[$product->id]->cover_img = $product->cover_img;
           
        }

        return $all_users_profiles;

    }


    public static function updatetheProduct(){
        $productId = strip_tags(Request::post('productId'));
        $brand_name = strip_tags(Request::post('brand_name'));
        $productName = strip_tags(Request::post('productName'));
        $sellerPrice = strip_tags(Request::post('sellerPrice'));
        $newPrice = strip_tags(Request::post('newPrice'));
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "UPDATE products SET product_title = :productName, admin_price = :admin_price
        WHERE id = :productId  LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(
        ':productId' => $productId, 
        ':productName' => $productName,
        ':admin_price' => $newPrice       
        ));

        // if one result exists, return true, else false. Could be written even shorter btw.
        return ($query->rowCount() == 1 ? true : false);
    }
    public static function getAllProduct($seller_id){


        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT * FROM products";
        $query = $database->prepare($sql);
        $query->execute();

        $all_user_products = array();

        foreach ($query->fetchAll() as $product) {

            // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
            // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
            // the user's values
            array_walk_recursive($product, 'Filter::XSSFilter');

            $all_users_profiles[$product->id] = new stdClass();
            $all_users_profiles[$product->id]->id = $product->id;
            $all_users_profiles[$product->id]->seller_id = $product->seller_id;
            $all_users_profiles[$product->id]->product_title = $product->product_title;
            $all_users_profiles[$product->id]->category = $product->category;
            $all_users_profiles[$product->id]->brand = $product->brand;
            $all_users_profiles[$product->id]->product_sku = $product->product_sku;
            $all_users_profiles[$product->id]->barcode = $product->barcode;
            $all_users_profiles[$product->id]->sizes = $product->sizes;
            $all_users_profiles[$product->id]->weight = $product->weight;
            $all_users_profiles[$product->id]->seller_price = $product->seller_price;
            $all_users_profiles[$product->id]->country = $product->country;
           
        }

        return $all_users_profiles;

    }

    //update product model
    public static function updateProducts(){

        $brand = strip_tags(Request::post('brand_name'));
        $product_title = strip_tags(Request::post('product_name'));
        $price = strip_tags(Request::post('price'));
        $country_id = strip_tags(Request::post('country'));
        $category = strip_tags(Request::post('category'));
        $weight = strip_tags(Request::post('weight'));
        $size = strip_tags(Request::post('size'));
        $product_sku = strip_tags(Request::post('sku'));
        $barcode = strip_tags(Request::post('barcode'));
        $pid = strip_tags(Request::post('pid'));

        if($brand=="" || $product_title=="" ||  $price=="" || $country_id=="" 
        || $category=="" 
        || $weight=="" 
        || $size=="" 
        || $product_sku=="" 
        || $barcode=="" 

         ){
           $data=array(
               'label'=>'danger',
               'txt'=>Text::get('PRODUCT_ADDED_ERROR_MESSAGE'),
               'st'=>'error'
           );
          // return   json_encode($data);
        }

         
       if (!self::UpdateProductToDatabase(11, $product_title, $category,$brand,$product_sku,$weight,$size,$price,$country_id,$barcode,$pid)) {
        $data=array(
            'label'=>'danger',
            'txt'=>Text::get('PRODUCT_ADDED_ERROR_MESSAGE'),
            'st'=>'success'
        );
       }else{
        $data=array(
            'label'=>'success',
            'txt'=>Text::get('PRODUCT_ADDED_SUCCESS_MESSAGE'),
            'st'=>'success'
        );

       }
      
       return   json_encode($data);

       

    }
    //update product model

    public static function saveProduct()
    {
        $ajpath='';

        if(isset($_FILES["file"]["type"]))
{
$validextensions = array("jpeg", "jpg", "png");
$temporary = explode(".", $_FILES["file"]["name"]);
$file_extension = end($temporary);
if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || ($_FILES["file"]["type"] == "image/jpeg")
) && ($_FILES["file"]["size"] < 1000000)//Approx. 100kb files can be uploaded.
&& in_array($file_extension, $validextensions)) {
if ($_FILES["file"]["error"] > 0)
{
    //echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
}
else
{
if (file_exists("upload/" . $_FILES["file"]["name"])) {
//echo $_FILES["file"]["name"] . " <span id='invalid'><b>already exists.</b></span> ";
}
else
{
$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
$targetPath = "upload/".$_FILES['file']['name']; // Target path where file is to be stored
move_uploaded_file($sourcePath,$targetPath) ; // Moving Uploaded file
// echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
// echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
// echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
// echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
// echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";
$ajpath=$targetPath;

}
}
}
else
{
    //echo "<span id='invalid'>***Invalid file Size or Type***<span>";
}
}


         $brand = strip_tags(Request::post('brand_name'));     

         $product_title = strip_tags(Request::post('product_name'));
         $seller_price = strip_tags(Request::post('seller_price'));
         $country_id = strip_tags(Request::post('country'));
         $category = strip_tags(Request::post('category'));
         $weight = strip_tags(Request::post('weight'));
         $size = strip_tags(Request::post('size'));
         $product_sku = strip_tags(Request::post('sku'));
         $barcode = strip_tags(Request::post('barcode'));

         if($brand=="" || $product_title=="" ||  $seller_price=="" || $country_id=="" 
         || $category=="" 
         || $weight=="" 
         || $size=="" 
         || $product_sku=="" 
         || $barcode=="" 

          ){
            $data=array(
                'label'=>'danger',
                'txt'=>Text::get('PRODUCT_ADDED_ERROR_MESSAGE'),
                'st'=>'error'
            );
           // return   json_encode($data);
         }
        
          $coverImg=$ajpath;
         

       if (!self::writeNewProductToDatabase(11, $product_title, $category,$brand,$product_sku,$weight,$size,$seller_price,$country_id,$barcode,$coverImg)) {
        $data=array(
            'label'=>'danger',
            'txt'=>Text::get('PRODUCT_ADDED_ERROR_MESSAGE'),
            'st'=>'success'
        );
       }else{
        $data=array(
            'label'=>'success',
            'txt'=>Text::get('PRODUCT_ADDED_SUCCESS_MESSAGE'),
            'st'=>'success'
        );

       }
      
       return   json_encode($data);

        
    }

    public static function UpdateProductToDatabase($seller_id, $product_title, $category, $brand, $product_sku,
    $weight,
    $size,
    $seller_price,
    $country_id,
    $barcode,
    $pid
    ){
        $database = DatabaseFactory::getFactory()->getConnection();
        $data = [
            'product_title' => $product_title,
            'barcode' => $barcode,
            'category' => $category,
            'brand' => $brand,
            'product_sku' => $product_sku,           
            'weight' => $weight,           
            'seller_price' => $seller_price,           
            'country_id' => $country_id,           
            'sizes' => $size,           
            'id' => $pid
           
        ];
        $sql = "UPDATE products SET product_title=:product_title,
         barcode=:barcode ,
         category=:category ,
         brand=:brand ,
         product_sku=:product_sku,        
         weight=:weight,        
         seller_price=:seller_price,      
         sizes=:sizes,      
         country=:country_id        
         WHERE id=:id";
        $query= $database->prepare($sql);
        $query->execute($data);

         $count =  $query->rowCount();
        

        if ($count == 1) {
            return true;
        }
        return false;




    }

   //sku
   public static function productSKU($seller_id,$product_title,$country_id){
    $str_length = 4;
    $sid = "P".substr("0000{$seller_id}", -$str_length); 
    $pt = "PT".substr($product_title, 1, 2);
    $cid = "C".substr("0000{$country_id}", -$str_length); 
    return $sid.strtoupper($pt).$cid;
   }
   //sku

    //==========================
    public static function writeNewProductToDatabase($seller_id, $product_title, $category, $brand, $product_sku,
    $weight,
    $size,
    $price,
    $country_id,
    $barcode,
    $coverImg

    )
    {
        $database = DatabaseFactory::getFactory()->getConnection();
        $psku=self::productSKU($seller_id,$product_title,$country_id);
        // write new product data into database
        $sql = "INSERT INTO products (seller_id, product_title, category, brand, product_sku,weight,sizes,seller_price,country,barcode,cover_img)
                    VALUES (:seller_id, :product_title, :category, :brand, :product_sku,:weight,:sizes,:seller_price,:country,:barcode,:cover_img)";
        $query = $database->prepare($sql);
        $query->execute(array(':seller_id' => $seller_id,
                              ':product_title' => $product_title,
                              ':category' => $category,
                              ':brand' => $brand,
                              ':product_sku' => $psku,
                              ':weight' => $weight,
                              ':sizes' => $size,
                              ':seller_price' => $price,
                              ':country' => $country_id,
                              ':barcode' => $barcode,
                              ':cover_img' => $coverImg

                             
                            ));
        $count =  $query->rowCount();
        if ($count == 1) {
            return true;
        }

        return false;
    }

   

    
    

    
}
