<?php

/**
 * Handles all data manipulation of the admin part
 */
class AdminModel
{
    /**
     * Sets the deletion and suspension values
     *
     * @param $suspensionInDays
     * @param $softDelete
     * @param $userId
     */
    public static function setAccountSuspensionAndDeletionStatus($suspensionInDays, $softDelete, $userId)
    {

        // Prevent to suspend or delete own account.
        // If admin suspend or delete own account will not be able to do any action.
        if ($userId == Session::get('user_id')) {
            Session::add('feedback_negative', Text::get('FEEDBACK_ACCOUNT_CANT_DELETE_SUSPEND_OWN'));
            return false;
        }

        if ($suspensionInDays > 0) {
            $suspensionTime = time() + ($suspensionInDays * 60 * 60 * 24);
        } else {
            $suspensionTime = null;
        }

        // FYI "on" is what a checkbox delivers by default when submitted. Didn't know that for a long time :)
        if ($softDelete == "on") {
            $delete = 1;
        } else {
            $delete = 0;
        }

        // write the above info to the database
        self::writeDeleteAndSuspensionInfoToDatabase($userId, $suspensionTime, $delete);

        // if suspension or deletion should happen, then also kick user out of the application instantly by resetting
        // the user's session :)
        if ($suspensionTime != null OR $delete = 1) {
            self::resetUserSession($userId);
        }
    }

    /**
     * Simply write the deletion and suspension info for the user into the database, also puts feedback into session
     *
     * @param $userId
     * @param $suspensionTime
     * @param $delete
     * @return bool
     */
    private static function writeDeleteAndSuspensionInfoToDatabase($userId, $suspensionTime, $delete)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET user_suspension_timestamp = :user_suspension_timestamp, user_deleted = :user_deleted  WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(
                ':user_suspension_timestamp' => $suspensionTime,
                ':user_deleted' => $delete,
                ':user_id' => $userId
        ));

        if ($query->rowCount() == 1) {
            Session::add('feedback_positive', Text::get('FEEDBACK_ACCOUNT_SUSPENSION_DELETION_STATUS'));
            return true;
        }
    }

    /**
     * Kicks the selected user out of the system instantly by resetting the user's session.
     * This means, the user will be "logged out".
     *
     * @param $userId
     * @return bool
     */
    private static function resetUserSession($userId)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET session_id = :session_id  WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(
                ':session_id' => null,
                ':user_id' => $userId
        ));

        if ($query->rowCount() == 1) {
            Session::add('feedback_positive', Text::get('FEEDBACK_ACCOUNT_USER_SUCCESSFULLY_KICKED'));
            return true;
        }
    }
    /**
     * To get Admin profile information and update them for myaccount
     * @param $userId
     * @return bool
     */
    public static function getAdminData()
    {
        $database = DatabaseFactory::getFactory()->getConnection();
        $userId = Session::get('user_id');
        $sql = "SELECT * FROM users Where user_id = $userId";
        $query = $database->prepare($sql);
        $query->execute();
        $admin = array();

        foreach ($query->fetchAll() as $user) {
            array_walk_recursive($user, 'Filter::XSSFilter');
            $admin[$user->user_id] = new stdClass();
            $admin[$user->user_id]->user_id = $user->user_id;
            $admin[$user->user_id]->user_email = $user->user_email;
            $admin[$user->user_id]->firstname = $user->firstname;            
            $admin[$user->user_id]->lastname = $user->lastname;   
            $admin[$user->user_id]->user_phone = $user->user_phone;                        
        }
        return $admin;
    }
    public static function getventData()
    {
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM vent";
        $query = $database->prepare($sql);
        $query->execute();

        $vent = array();

        foreach ($query->fetchAll() as $user) {
            array_walk_recursive($user, 'Filter::XSSFilter');
            $vent[$user->id] = new stdClass();
            $vent[$user->id]->id = $user->id;
            $vent[$user->id]->message = $user->vent_message;
            $vent[$user->id]->date = $user->added_on;            
        }

        return $vent;
    }
    public static function updateAdmin(){
      
        $user_id=Session::get('user_id');
        $firstname=Request::post('firtstname');
        $lastname=Request::post('lastname');
        $phone=Request::post('phone');
        $email=Request::post('email');

        $reTypePassword=Request::post('reTypePassword');
        $user_address=Request::post('user_address');        

        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET user_email = :user_email,
        firstname= :firstname, lastname = :lastname , user_phone = :user_phone WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(
            ':user_email' => $email,
            ':user_id' => $user_id,
            ':firstname' => $firstname,
            ':lastname' => $lastname,
            ':user_phone' => $phone
        ));
        $count = $query->rowCount();
      

       if ($count == 1) {
        //    $headers = array('Content-Type: text/html; charset=UTF-8');
        //    $body = file_get_contents(Config::get('URL'). 'email/email.php');
        //    $body = str_replace('{{--Email--}}', $user_email, $body);            
        //    $mail = new Mail;
        //    $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
        //        Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
        //    );

           $data=array(
               'label'=>'success',
               'txt'=>Text::get('PROFILE_UPDATE_MESSAGE'),
               'status'=>1,

           );         

       }else{
           $data=array(
               'label'=>'error',
               'txt'=>Text::get('PROFILE_UPDATE_FAILDED'),
               'status'=>0,

           );
            
       }   
       echo json_encode($data);
   }
    
}
