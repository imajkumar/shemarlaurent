<?php

/**
 * UserModel
 * Handles all the PUBLIC profile stuff. This is not for getting data of the logged in user, it's more for handling
 * data of all the other users. Useful for display profile information, creating user lists etc.
 */
class UserModel extends LoginModel
{
    /**
     * Gets an array that contains all the users in the database. The array's keys are the user ids.
     * Each array element is an object, containing a specific user's data.
     * The avatar line is built using Ternary Operators, have a look here for more:
     * @see http://davidwalsh.name/php-shorthand-if-else-ternary-operators
     *
     * @return array The profiles of all users
     */
    public static function paynow(){
        include('BluePay.php');
        $accountID = "100783021057";
        $secretKey = "WYZQAJ6V2VOLZ4YMYEASXEGZZTFUFEST";
        $mode = "TEST";
        $rebill = new BluePay(
            $accountID,
            $secretKey,
            $mode
        );
       

       

        $user_id = Session::get('user_id');

        $user_data=self::getPublicProfileOfUser($user_id);

        $card_holder_name = strip_tags(Request::post('card_holder_name'));
        $card_number = strip_tags(Request::post('card_number'));
        $card_exp = strip_tags(Request::post('card_exp'));
       



        $rebill->setCustomerInformation(array(
            'firstName' => $user_data->firstname, 
            'lastName' => 'Tester', 
            'addr1' => '1234 Test St.', 
            'addr2' => 'Apt500', 
            'city' => 'Testville', 
            'state' => 'IL', 
            'zip' =>'54321', 
            'country' => 'USA', 
            'phone' => '12312312348', 
            'email' => $user_data->user_email 
        ));
 
        $rebill->setCCInformation(array(
            'cardNumber' => '4111111111111111', // Card Number: 4111111111111111
            'cardExpire' => '1225', // Card Expire: 12/25
            'cvv2' => '123' // Card CVV2: 123
        ));
        $rebill->setRebillingInformation(array(
        'rebillFirstDate' => '2015-02-05', // Rebill Start Date: Jan. 5, 2015
        'rebillExpression' => '1 MONTH', // Rebill Frequency: 1 MONTH
        'rebillCycles' => '5', // Rebill # of Cycles: 5
        'rebillAmount' => '3.50' // Rebill Amount: $3.50
        ));
        $rebill->auth('0.00');
        // Makes the API Request to create a recurring payment
        $rebill->process();
        // If transaction was approved..
        if ($rebill->isSuccessfulResponse()) {
            $rebillStatus = new BluePay(
                $accountID,
                $secretKey,
                $mode);
            // Find the rebill by ID and get rebill status 
            $rebillStatus->getRebillStatus($rebill->getRebillID());
            // Makes the API Request to get the rebill status
            $rebillStatus->process();
            // Read response from BluePay
            echo 
            'Rebill Status: ' . $rebillStatus->getRebStatus() . "\n" .
            'Rebill ID: ' . $rebillStatus->getRebID() . "\n" .
            'Template ID: ' . $rebillStatus->getTemplateID() . "\n" .
            'Rebill Creation Date: ' . $rebillStatus->getCreationDate() . "\n" .
            'Rebill Next Date: ' . $rebillStatus->getNextDate() . "\n" .
            'Rebill Last Date: ' . $rebillStatus->getLastDate() . "\n" .
            'Rebill Expression: ' . $rebillStatus->getSchedExpr() . "\n" .
            'Rebill Cycles Remaining: ' . $rebillStatus->getCyclesRemaining() . "\n" .
            'Rebill Amount: ' . $rebillStatus->getRebAmount() . "\n" .
            'Rebill Next Amount Charged: ' . $rebillStatus->getNextAmount();
        } else {
            echo $rebill->getMessage();
        }



    }
    public static function affiliate_approved(){
        $affiliate_id=Request::post('affiliate_id');
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM affiliate_application WHERE id = :id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':id' => $affiliate_id));
        $data= $query->fetch();
        $user_email=$data->email;
      
        $pass_protect= password_hash('123456', PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (user_name, user_password_hash, user_email, user_account_type,user_provider_type)
        VALUES (:user_name, :user_password_hash, :user_email, :user_account_type,:user_provider_type)";
        $query = $database->prepare($sql);
        $query->execute(array(':user_name' => $data->name,
                 ':user_password_hash' => $pass_protect,
                 ':user_email' =>$user_email,                                                                        
                 ':user_account_type' => 6,
                 ':user_provider_type' => 'DEFAULT'));
            $count =  $query->rowCount();
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('REGISTRATION_SUCESS'),
                'status'=>'1'
            ); 

            $headers = array('Content-Type: text/html; charset=UTF-8');
            $body = file_get_contents(Config::get('URL'). 'email/affliate_approved.php');
            $body = str_replace('{{--Email--}}', $user_email, $body); 
            $body = str_replace('{{--Password--}}', '123456', $body); 

            $mail = new Mail;
            $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
                Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
            );

            echo json_encode($data);


    }

    public static function seller_approved(){
        $seller_id=Request::post('seller_id');
        $database = DatabaseFactory::getFactory()->getConnection();
        $sql = "SELECT * FROM seller_application WHERE id = :id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':id' => $seller_id));
        $data= $query->fetch();
        $user_email=$data->email;
      
        $pass_protect= password_hash('123456', PASSWORD_DEFAULT);
        $sql = "INSERT INTO users (user_name, user_password_hash, user_email, user_account_type,user_provider_type)
        VALUES (:user_name, :user_password_hash, :user_email, :user_account_type,:user_provider_type)";
        $query = $database->prepare($sql);
        $query->execute(array(':user_name' => $data->business_name,
                 ':user_password_hash' => $pass_protect,
                 ':user_email' =>$data->email,                             
                 ':user_account_type' => 5,
                 ':user_provider_type' => 'DEFAULT'));
            $count =  $query->rowCount();
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('REGISTRATION_SUCESS'),
                'status'=>'1'
            ); 

            $headers = array('Content-Type: text/html; charset=UTF-8');
            $body = file_get_contents(Config::get('URL'). 'email/seller_approved.php');
            $body = str_replace('{{--Email--}}', $user_email, $body); 
            $body = str_replace('{{--Password--}}', '123456', $body); 

            $mail = new Mail;
            $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
                Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
            );

            echo json_encode($data);


    }
    public static function getAllSellerUser()
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT * FROM seller_application";
        $query = $database->prepare($sql);
        $query->execute();

        $all_users_profiles = array();

        foreach ($query->fetchAll() as $user) {

            // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
            // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
            // the user's values
            array_walk_recursive($user, 'Filter::XSSFilter');

            $all_users_profiles[$user->id] = new stdClass();
            $all_users_profiles[$user->id]->id = $user->id;
            $all_users_profiles[$user->id]->business_name = $user->business_name;
            $all_users_profiles[$user->id]->website = $user->website;
            $all_users_profiles[$user->id]->email = $user->email;
            $all_users_profiles[$user->id]->about = $user->about;
            
        }

        return $all_users_profiles;
    }
    /*Get all Affiliate User*/
    public static function getAllAffiliateUser()
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT * FROM affiliate_application";
        $query = $database->prepare($sql);
        $query->execute();

        $all_users_profiles = array();

        foreach ($query->fetchAll() as $user) {

            // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
            // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
            // the user's values
            array_walk_recursive($user, 'Filter::XSSFilter');

            $all_users_profiles[$user->id] = new stdClass();
            $all_users_profiles[$user->id]->id = $user->id;
            $all_users_profiles[$user->id]->name = $user->name;
            $all_users_profiles[$user->id]->email = $user->email;
            $all_users_profiles[$user->id]->social_media_name = $user->social_media_name;
            $all_users_profiles[$user->id]->no_of_followers = $user->no_of_followers;
            $all_users_profiles[$user->id]->active_youtube = $user->active_youtube;
            $all_users_profiles[$user->id]->no_of_subscriber = $user->no_of_subscriber;
            $all_users_profiles[$user->id]->about = $user->about;
            
        }

        return $all_users_profiles;
    }

    public static function getSellerAuthcode(){
        $database = DatabaseFactory::getFactory()->getConnection();
        $unique_ref=Request::post('txtSellerCode');
        $sql = "SELECT code_index FROM core_code WHERE code_index = :code_index LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':code_index' => $unique_ref));

        if ($query->rowCount()>0) {
            $data=array(
                'label'=>'success',
                'txt'=>'Authorised Code',
                'status'=>'1'
            ); 

        }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('REGISTRATION_FAIL'),
                'status'=>'0'
            ); 
        }
        echo json_encode($data);

     }
     public static function getAuthCodeSave(){
       

        $database = DatabaseFactory::getFactory()->getConnection();
        $unique_ref=Request::post('seller_code');
        $sql = "SELECT code_index FROM core_code WHERE code_index = :code_index LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':code_index' => $unique_ref));
       
        if ($query->rowCount()>=0) {
           //save
            $sql = "INSERT INTO core_code (code_index)
            VALUES (:code_index)";
            $query = $database->prepare($sql);
            $query->execute(array(
                                ':code_index' => $unique_ref                    )
                            );
            $count =  $query->rowCount();
            if ($count == 1) {
            $data=array(
                'label'=>'success',
                'txt'=>Text::get('REGISTRATION_SUCESS'),
                'status'=>'1'
            ); 
            }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('REGISTRATION_FAIL'),
                'status'=>'0'
            ); 
            }
            echo json_encode($data);
                    //save

        }else{

            

        }
        

        
     }
     public static function getAuthCode(){
        $database = DatabaseFactory::getFactory()->getConnection();
   

        // The length we want the unique reference number to be
$unique_ref_length = 10;

// A true/false variable that lets us know if we've
// found a unique reference number or not
$unique_ref_found = false;

// Define possible characters.
// Notice how characters that may be confused such
// as the letter 'O' and the number zero don't exist
$possible_chars = "23456789BCDFGHJKMNPQRSTVWXYZ";

// Until we find a unique reference, keep generating new ones
while (!$unique_ref_found) {

	// Start with a blank reference number
	$unique_ref = "";
	
	// Set up a counter to keep track of how many characters have 
	// currently been added
	$i = 0;
	
	// Add random characters from $possible_chars to $unique_ref 
	// until $unique_ref_length is reached
	while ($i < $unique_ref_length) {
	
		// Pick a random character from the $possible_chars list
		$char = substr($possible_chars, mt_rand(0, strlen($possible_chars)-1), 1);
		
		$unique_ref .= $char;
		
		$i++;
	
	}
	
	// Our new unique reference number is generated.
    // Lets check if it exists or not
    $sql = "SELECT code_index FROM core_code WHERE code_index = :code_index LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':code_index' => $unique_ref));

	
	if ($query->rowCount()==0) {
	
		// We've found a unique number. Lets set the $unique_ref_found
		// variable to true and exit the while loop
		$unique_ref_found = true;
	
	}

}

echo $unique_ref;




      

        

     }
    public static function sendEmailVerify(){
        $user_id=Request::post('user_id');
        $firstname=Request::post('firstname');
        $lastname=Request::post('lastname');
        $oldPassword=Request::post('oldPassword');
        $reTypePassword=Request::post('reTypePassword');
        $user_address=Request::post('user_address');
        $user_email=Request::post('email');

        
        $user_activation_hash = sha1(uniqid(mt_rand(), true));

      
        // get user_id of the user that has been created, to keep things clean we DON'T use lastInsertId() here
        $user_id = UserModel::getUserIdByEmail($user_email);

        if (!$user_id) {
            Session::add('feedback_negative', Text::get('FEEDBACK_UNKNOWN_ERROR'));
            return false;
        }

        // send verification email
        if (RegistrationModel::sendVerificationEmail($user_id, $user_email, $user_activation_hash)) {
           
            Session::add('feedback_positive', Text::get('FEEDBACK_ACCOUNT_SUCCESSFULLY_CREATED'));
             $database = DatabaseFactory::getFactory()->getConnection();
           
             $query = $database->prepare("UPDATE users SET user_activation_hash = :user_activation_hash WHERE user_id = :user_id LIMIT 1");
            $query->execute(array(':user_activation_hash' => $user_activation_hash, ':user_id' => $user_id));
            if ($query->rowCount() == 1) {
                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('SEND_EMAIL_VERIFY'),
                    'status'=>1,
    
                );
    
               echo    json_encode($data);
            }
            
            
        }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('PROFILE_UPDATE_FAILDED'),
                'status'=>0,

            );

           echo    json_encode($data);
        }

    }
    public static function getPublicProfilesOfAllUsers()
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT user_id, user_name, user_email, user_active, user_has_avatar, user_deleted FROM users";
        $query = $database->prepare($sql);
        $query->execute();

        $all_users_profiles = array();

        foreach ($query->fetchAll() as $user) {

            // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
            // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
            // the user's values
            array_walk_recursive($user, 'Filter::XSSFilter');

            $all_users_profiles[$user->user_id] = new stdClass();
            $all_users_profiles[$user->user_id]->user_id = $user->user_id;
            $all_users_profiles[$user->user_id]->user_name = $user->user_name;
            $all_users_profiles[$user->user_id]->user_email = $user->user_email;
            $all_users_profiles[$user->user_id]->user_active = $user->user_active;
            $all_users_profiles[$user->user_id]->user_deleted = $user->user_deleted;
            $all_users_profiles[$user->user_id]->user_avatar_link = (Config::get('USE_GRAVATAR') ? AvatarModel::getGravatarLinkByEmail($user->user_email) : AvatarModel::getPublicAvatarFilePathOfUser($user->user_has_avatar, $user->user_id));
        }

        return $all_users_profiles;
    }
    public static function getMemberPlans()
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT *  FROM member_type";
        $query = $database->prepare($sql);
        $query->execute();
        
        $all_users_profiles = array();

        foreach ($query->fetchAll() as $user) {

            
            array_walk_recursive($user, 'Filter::XSSFilter');

            $all_users_profiles[$user->member_id] = new stdClass();
            $all_users_profiles[$user->member_id]->member_id = $user->member_id;
            $all_users_profiles[$user->member_id]->member_type = $user->member_type;
            $all_users_profiles[$user->member_id]->is_active = $user->is_active;
            
        }

        return $all_users_profiles;

      
       
    }
    public static function  MyAccount(){

    }
    public static function getMemberPlansDetails()
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT *  FROM member_features_name";
        $query = $database->prepare($sql);
        $query->execute();        
        $all_users_profiles = array();
       

        foreach ($query->fetchAll() as $user) {

             //----------------------------------
        $sql_1 = "SELECT *
        FROM member_features WHERE plan_code = :plan_code";
        $query_1 = $database->prepare($sql_1);
        $query_1->execute(array(':plan_code' => $user->m_code));
        $mydata=$query_1->fetchAll();      

      
        //----------------------------------

            
            array_walk_recursive($user, 'Filter::XSSFilter');

            $all_users_profiles[$user->id] = new stdClass();
            $all_users_profiles[$user->id]->m_code = $user->m_code;
            $all_users_profiles[$user->id]->name = $user->name;
            $all_users_profiles[$user->id]->is_active = $user->is_active;
            $all_users_profiles[$user->id]->plan_data = $mydata;
            
        }

        return $all_users_profiles;

      
       
    }


    

    /**
     * Gets a user's profile data, according to the given $user_id
     * @param int $user_id The user's id
     * @return mixed The selected user's profile
     */
    public static function getPublicProfileOfUser($user_id)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT user_id, user_name,firstname,lastname,address, user_email, user_active, user_has_avatar, user_deleted
                FROM users WHERE user_id = :user_id LIMIT 1";
        $query = $database->prepare($sql);
        $query->execute(array(':user_id' => $user_id));

        $user = $query->fetch();

        if ($query->rowCount() == 1) {
            if (Config::get('USE_GRAVATAR')) {
                $user->user_avatar_link = AvatarModel::getGravatarLinkByEmail($user->user_email);
            } else {
                $user->user_avatar_link = AvatarModel::getPublicAvatarFilePathOfUser($user->user_has_avatar, $user->user_id);
            }
        } else {
            Session::add('feedback_negative', Text::get('FEEDBACK_USER_DOES_NOT_EXIST'));
        }

        // all elements of array passed to Filter::XSSFilter for XSS sanitation, have a look into
        // application/core/Filter.php for more info on how to use. Removes (possibly bad) JavaScript etc from
        // the user's values
        array_walk_recursive($user, 'Filter::XSSFilter');

        return $user;
    }

    /**
     * @param $user_name_or_email
     *
     * @return mixed
     */
    public static function getUserDataByUserNameOrEmail($user_name_or_email)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("SELECT user_id, user_name, user_email FROM users
                                     WHERE (user_name = :user_name_or_email OR user_email = :user_name_or_email)
                                           AND user_provider_type = :provider_type LIMIT 1");
        $query->execute(array(':user_name_or_email' => $user_name_or_email, ':provider_type' => 'DEFAULT'));

        return $query->fetch();
    }

    /**
     * Checks if a username is already taken
     *
     * @param $user_name string username
     *
     * @return bool
     */
    public static function doesUsernameAlreadyExist($user_name)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("SELECT user_id FROM users WHERE user_name = :user_name LIMIT 1");
        $query->execute(array(':user_name' => $user_name));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    /**
     * Checks if a email is already used
     *
     * @param $user_email string email
     *
     * @return bool
     */
    public static function doesEmailAlreadyExist($user_email)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("SELECT user_id FROM users WHERE user_email = :user_email LIMIT 1");
        $query->execute(array(':user_email' => $user_email));
        if ($query->rowCount() == 0) {
            return false;
        }
        return true;
    }

    /**
     * Writes new username to database
     *
     * @param $user_id int user id
     * @param $new_user_name string new username
     *
     * @return bool
     */
    public static function saveNewUserName($user_id, $new_user_name)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET user_name = :user_name WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(':user_name' => $new_user_name, ':user_id' => $user_id));
        if ($query->rowCount() == 1) {
            return true;
        }
        return false;
    }

    /**
     * Writes new email address to database
     *
     * @param $user_id int user id
     * @param $new_user_email string new email address
     *
     * @return bool
     */
    public static function setUserProfileUpdate(){
      
         $user_id=Request::post('user_id');
         $firstname=Request::post('firstname');
         $lastname=Request::post('lastname');
         $oldPassword=Request::post('oldPassword');
         $reTypePassword=Request::post('reTypePassword');
         $user_address=Request::post('user_address');
         $user_email=Request::post('email');

        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET address= :user_address ,user_email = :user_email,firstname= :firstname ,lastname =:lastname WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(':user_email' => $user_email, ':user_id' => $user_id
            , ':firstname' => $firstname
            , ':lastname' => $lastname
            , ':user_address' => $user_address
        ));
        $count = $query->rowCount();
       

        if ($count == 1) {
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $body = file_get_contents(Config::get('URL'). 'email/email.php');
            $body = str_replace('{{--Email--}}', $user_email, $body);            
            $mail = new Mail;
            $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
                Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
            );

            $data=array(
                'label'=>'error',
                'txt'=>Text::get('PROFILE_UPDATE_MESSAGE'),
                'status'=>1,

            );

           echo    json_encode($data);

        }else{
            $data=array(
                'label'=>'error',
                'txt'=>Text::get('PROFILE_UPDATE_FAILDED'),
                'status'=>1,

            );

        echo    json_encode($data);
        }   
  
    }
    public static function saveNewEmailAddress($user_id, $new_user_email)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $query = $database->prepare("UPDATE users SET user_email = :user_email WHERE user_id = :user_id LIMIT 1");
        $query->execute(array(':user_email' => $new_user_email, ':user_id' => $user_id));
        $count = $query->rowCount();
        if ($count == 1) {
            return true;
        }
        return false;
    }

    /**
     * Edit the user's name, provided in the editing form
     *
     * @param $new_user_name string The new username
     *
     * @return bool success status
     */
    public static function editUserName($new_user_name)
    {
        // new username same as old one ?
        if ($new_user_name == Session::get('user_name')) {
            Session::add('feedback_negative', Text::get('FEEDBACK_USERNAME_SAME_AS_OLD_ONE'));
            return false;
        }

        // username cannot be empty and must be azAZ09 and 2-64 characters
        if (!preg_match("/^[a-zA-Z0-9]{2,64}$/", $new_user_name)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_USERNAME_DOES_NOT_FIT_PATTERN'));
            return false;
        }

        // clean the input, strip usernames longer than 64 chars (maybe fix this ?)
        $new_user_name = substr(strip_tags($new_user_name), 0, 64);

        // check if new username already exists
        if (self::doesUsernameAlreadyExist($new_user_name)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_USERNAME_ALREADY_TAKEN'));
            return false;
        }

        $status_of_action = self::saveNewUserName(Session::get('user_id'), $new_user_name);
        if ($status_of_action) {
            Session::set('user_name', $new_user_name);
            Session::add('feedback_positive', Text::get('FEEDBACK_USERNAME_CHANGE_SUCCESSFUL'));
            return true;
        } else {
            Session::add('feedback_negative', Text::get('FEEDBACK_UNKNOWN_ERROR'));
            return false;
        }
    }

    /**
     * Edit the user's email
     *
     * @param $new_user_email
     *
     * @return bool success status
     */
    public static function editUserEmail($new_user_email)
    {
        // email provided ?
        if (empty($new_user_email)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_EMAIL_FIELD_EMPTY'));
            return false;
        }

        // check if new email is same like the old one
        if ($new_user_email == Session::get('user_email')) {
            Session::add('feedback_negative', Text::get('FEEDBACK_EMAIL_SAME_AS_OLD_ONE'));
            return false;
        }

        // user's email must be in valid email format, also checks the length
        // @see http://stackoverflow.com/questions/21631366/php-filter-validate-email-max-length
        // @see http://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
        if (!filter_var($new_user_email, FILTER_VALIDATE_EMAIL)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_EMAIL_DOES_NOT_FIT_PATTERN'));
            return false;
        }

        // strip tags, just to be sure
        $new_user_email = substr(strip_tags($new_user_email), 0, 254);

        // check if user's email already exists
        if (self::doesEmailAlreadyExist($new_user_email)) {
            Session::add('feedback_negative', Text::get('FEEDBACK_USER_EMAIL_ALREADY_TAKEN'));
            return false;
        }

        // write to database, if successful ...
        // ... then write new email to session, Gravatar too (as this relies to the user's email address)
        if (self::saveNewEmailAddress(Session::get('user_id'), $new_user_email)) {
            Session::set('user_email', $new_user_email);
            Session::set('user_gravatar_image_url', AvatarModel::getGravatarLinkByEmail($new_user_email));
            Session::add('feedback_positive', Text::get('FEEDBACK_EMAIL_CHANGE_SUCCESSFUL'));
            return true;
        }

        Session::add('feedback_negative', Text::get('FEEDBACK_UNKNOWN_ERROR'));
        return false;
    }

    /**
     * Gets the user's id
     *
     * @param $user_name
     *
     * @return mixed
     */
    public static function getUserIdByUsername($user_name)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT user_id FROM users WHERE user_name = :user_name AND user_provider_type = :provider_type LIMIT 1";
        $query = $database->prepare($sql);

        // DEFAULT is the marker for "normal" accounts (that have a password etc.)
        // There are other types of accounts that don't have passwords etc. (FACEBOOK)
        $query->execute(array(':user_name' => $user_name, ':provider_type' => 'DEFAULT'));

        // return one row (we only have one result or nothing)
        return $query->fetch()->user_id;
    }
    public static function getUserIdByEmail($user_email)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT user_id FROM users WHERE user_email = :user_email LIMIT 1";
        $query = $database->prepare($sql);

        // DEFAULT is the marker for "normal" accounts (that have a password etc.)
        // There are other types of accounts that don't have passwords etc. (FACEBOOK)
        $query->execute(array(':user_email' => $user_email));

        // return one row (we only have one result or nothing)
        return $query->fetch()->user_id;
    }

    /**
     * Gets the user's data
     *
     * @param $user_name string User's name
     *
     * @return mixed Returns false if user does not exist, returns object with user's data when user exists
     */
    public static function getUserDataByUsername($user_name)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        $sql = "SELECT user_id, user_name, user_email, user_password_hash, user_active,user_deleted, user_suspension_timestamp, user_account_type,
                       user_failed_logins, user_last_failed_login
                  FROM users
                 WHERE (user_name = :user_name OR user_email = :user_name)
                       AND user_provider_type = :provider_type
                 LIMIT 1";
        $query = $database->prepare($sql);

        // DEFAULT is the marker for "normal" accounts (that have a password etc.)
        // There are other types of accounts that don't have passwords etc. (FACEBOOK)
        $query->execute(array(':user_name' => $user_name, ':provider_type' => 'DEFAULT'));

        // return one row (we only have one result or nothing)
        return $query->fetch();
    }

    /**
     * Gets the user's data by user's id and a token (used by login-via-cookie process)
     *
     * @param $user_id
     * @param $token
     *
     * @return mixed Returns false if user does not exist, returns object with user's data when user exists
     */
    public static function getUserDataByUserIdAndToken($user_id, $token)
    {
        $database = DatabaseFactory::getFactory()->getConnection();

        // get real token from database (and all other data)
        $query = $database->prepare("SELECT user_id, user_name, user_email, user_password_hash, user_active,
                                          user_account_type,  user_has_avatar, user_failed_logins, user_last_failed_login
                                     FROM users
                                     WHERE user_id = :user_id
                                       AND user_remember_me_token = :user_remember_me_token
                                       AND user_remember_me_token IS NOT NULL
                                       AND user_provider_type = :provider_type LIMIT 1");
        $query->execute(array(':user_id' => $user_id, ':user_remember_me_token' => $token, ':provider_type' => 'DEFAULT'));

        // return one row (we only have one result or nothing)
        return $query->fetch();
    }

    public static function updateUser(){
      
        $user_id=Session::get('user_id');
        $user_name= Session::get('user_name');
        $firstname=Request::post('firtstname');
        $lastname=Request::post('lastname');
        $phone=Request::post('phone');
        $email=Request::post('email');

        $new_password=Request::post('new_password');
        $current_password=Request::post('current_password');
        $database = DatabaseFactory::getFactory()->getConnection();
        if($new_password !="" && $current_password !=""){
            // checks if user exists, if login is not blocked (due to failed logins) and if password fits the hash
            $result = LoginModel::validateAndGetUser($user_name, $current_password);

            // check if that user exists. We don't give back a cause in the feedback to avoid giving an attacker details.
            if (!$result) {
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('PROFILE_UPDATE_PASS_MESSAGE'),
                    'status'=>2
                );  
            }else{

                $user_password_hash = password_hash($new_password, PASSWORD_DEFAULT);

                $sql = "UPDATE users SET user_password_hash = :user_password_hash,  
                        user_email = :user_email,
                        firstname= :firstname, lastname = :lastname , user_phone = :user_phone
                        WHERE user_name = :user_name
                        AND user_provider_type = :user_provider_type LIMIT 1";
                $query = $database->prepare($sql);                
                $query->execute(array(
                    'user_name'=>$user_name,
                    ':user_password_hash' => $user_password_hash, 
                    ':user_provider_type' => 'DEFAULT',
                    ':user_email' => $email,                    
                    ':firstname' => $firstname,
                    ':lastname' => $lastname,
                    ':user_phone' => $phone
                ));
                $count = $query->rowCount();
            

                 if ($count == 1) {
                //    $headers = array('Content-Type: text/html; charset=UTF-8');
                //    $body = file_get_contents(Config::get('URL'). 'email/email.php');
                //    $body = str_replace('{{--Email--}}', $user_email, $body);            
                //    $mail = new Mail;
                //    $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
                //        Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
                //    );

                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('PROFILE_UPDATE_PASS_MESSAGE'),
                    'status'=>3
                );        

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('PROFILE_UPDATE_FAILDED'),
                    'status'=>0,

                );
                    
            }   
               
            }

        }else{


                $query = $database->prepare("UPDATE users SET user_email = :user_email,
                firstname= :firstname, lastname = :lastname , user_phone = :user_phone WHERE user_id = :user_id LIMIT 1");
                $query->execute(array(
                    ':user_email' => $email,
                    ':user_id' => $user_id,
                    ':firstname' => $firstname,
                    ':lastname' => $lastname,
                    ':user_phone' => $phone
                ));
                $count = $query->rowCount();
            

            if ($count == 1) {
                //    $headers = array('Content-Type: text/html; charset=UTF-8');
                //    $body = file_get_contents(Config::get('URL'). 'email/email.php');
                //    $body = str_replace('{{--Email--}}', $user_email, $body);            
                //    $mail = new Mail;
                //    $mail_sent = $mail->sendMail($user_email, Config::get('NO_REPLAY'),
                //        Config::get('NO_REPLAY'), Config::get('PROFILE_UPDATE_SUBJECT'), $body,$headers
                //    );

                $data=array(
                    'label'=>'success',
                    'txt'=>Text::get('PROFILE_UPDATE_MESSAGE'),
                    'status'=>1,

                );         

            }else{
                $data=array(
                    'label'=>'error',
                    'txt'=>Text::get('PROFILE_UPDATE_FAILDED'),
                    'status'=>0,

                );
                    
            }   
        }
        echo json_encode($data);
   }

  

}
