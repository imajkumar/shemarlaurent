$('document').ready(function () {
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: false,
        mouseDrag: false,
        autoplay: true,
        autoplayTimeout: 8000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    })
});


$('document').ready(function () {
    if( window.location.href != BASE_URL){
        $('.stall').hide();
    }     

    $('.signin').click(function () {
        $('.login').slideDown();
    });
    $('.login .overlay').click(function () {
        $('.login').slideUp();
    });


    $('.search_icon').click(function () {
        $('.search').fadeIn();
    });
    $('.search .overlay').click(function () {
        $('.search').fadeOut();
    });


    $('.affalite_btn').click(function () {
        $('.affiliate').fadeIn();
    });
    $('.affiliate .overlay').click(function () {
        $('.affiliate').fadeOut();
    });


    $('.seller_btn').click(function () {
        $('.seller').fadeIn();
    });
    $('.seller .overlay').click(function () {
        $('.seller').fadeOut();
    });

    $('.product-area .top-bar img').click(function () {
        $('.icon_bar').fadeIn();
    });
    $('.icon_bar .overlay').click(function () {
        $('.icon_bar').fadeOut();
    });

    $('.stall .overlay').click(function () {
        $('.stall').fadeOut();
    });


    $('.expendli').click(function () {
        $('.expend').fadeIn();
    });
    $('.expend img').click(function () {
        $('.expend').fadeOut();
        $('.icon_bar').hide();
    });


    $('.cart_area .cont button').click(function () {
        $('.thank_popup').fadeIn();
    });
    $('.thank_popup .overlay').click(function () {
        $('.thank_popup').fadeOut();
    });

    $('#yes').click(function(){
        $('.what_city').slideDown();
    });
    $('#no').click(function(){
        $('.what_city').slideUp();
    });

    $('#fyes').click(function(){
        $('.followers').slideDown();
    });
    $('#fno').click(function(){
        $('.followers').slideUp();
    });


     $('.basic').click(function(){
        $('.basic').addClass('active');
        $('.standard, .premium').removeClass('active');
    });
    $('.standard').click(function(){
        $(this).addClass('active');
        $('.basic, .premium').removeClass('active');
    });
    $('.premium').click(function(){
        $(this).toggleClass('active');
        $('.standard, .basic').removeClass('active');
    });
});



jQuery(function ($) {
    $(document).ajaxSend(function () {
        $(".loader").fadeIn(30);
    });

    $('.registration_area button').click(function () {
        $.ajax({
            type: 'GET',
            success: function (data) {
                console.log(data);
            }
        }).done(function () {
            setTimeout(function () {
                $(".loader").fadeOut(30);
            }, 50);
        });
    });

//txtSearch

$('#txtSearch').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        alert('You pressed a "enter" key in textbox');  
    }
});

//txtSearch




    //btnSendEmailVerification
    $('#btnSendEmailVerification').click(function(){
        var formData = {
            'firstname':$('#firstname').val(),
            'user_id':$('#user_id').val(),
            'lastname':$('#lastname').val(),
            'oldPassword':$('#oldPassword').val(),
            'newPassword':$('#newPassword').val(),
            'reTypePassword':$('#reTypePassword').val(),
            'user_address':$('#user_address').val(),
            'email':$('#email').val(),
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
         };
        //  ajax call
         //  ajax call
         $.ajax({
            url: BASE_URL+'/profile/sendEmailVerify',
            type: 'POST',
            data: formData,
            success: function(res) {
            
             if(res.status==1){
                toastr.success(res.txt)
             }
             if(res.status==0){
                toastr.error(res.txt)
             }
            },
            dataType : 'json'
        });
        //  ajax call


    })
    //btnSendEmailVerification
    //btnUpdateUserProfile
    $('#btnUpdateUserProfile').click(function(){

        

        var formData = {
            'firstname':$('#firstname').val(),
            'user_id':$('#user_id').val(),
            'lastname':$('#lastname').val(),
            'oldPassword':$('#oldPassword').val(),
            'newPassword':$('#newPassword').val(),
            'reTypePassword':$('#reTypePassword').val(),
            'user_address':$('#user_address').val(),
            'email':$('#email').val(),
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
         };
        //  ajax call
        $.ajax({
            url: BASE_URL+'/profile/update',
            type: 'POST',
            data: formData,
            success: function(res) {
             console.log(res);
             if(res.status){
                toastr.success(res.txt)
             }
             if(res.status==0){
                toastr.error(res.txt)
             }

            },
            dataType : 'json'
        });
        //  ajax call

        
         
    });
    //btnUpdateUserProfile

    

    //btnChangeEmail
     $('#btnChangeEmail').click(function(){
        $('.email_view').css('display','block');
        $('.emailView').css('border', '2px solid #000000');
     });
    //btnChangeEmail
    //btnChangePassword
    $('#btnChangePassword').click(function(){
        $('.password').css('display','block');
        $('.passwordview').css('border', '2px solid #000000');
     });
    //btnChangePassword

    // chnage addrees
    $('.email_view').css('display','none');
    $('.password').css('display','none');

    $('#btnChanageAddress').click(function(){        
        $('#user_address').attr('readonly', false);
        $('#user_address').css('border', '2px solid #000000');
        
    });
    // chnage addrees
    //btnLogin
    $('#btnLogin').click(function(){

    var user_name=$('#user_name').val();
    var user_password=$('#user_password').val();

      var formData = {
          'user_name':user_name,
          'user_password':user_password,
          'csrf_token':$('meta[name="csrf_token"]').attr('content')
       };

     $.ajax({
         url: BASE_URL+'/login/login',
         type: 'POST',
         data: formData,
         success: function(res) {
          console.log(res);
         // return false;
          if(res.status){
            if(res.user_type==7){
              window.location.href = BASE_URL+'/admin/index';
            }
            if(res.user_type==5){
                window.location.href = BASE_URL+'/seller/index';
             }
             if(res.user_type==1){
                window.location.href = BASE_URL+'/index/myaccount';
             }
             if(res.user_type==6){
                window.location.href = BASE_URL+'/affiliate/index';
             }
          }
            if(res.status==0){
             
              toastr.error('Invalid Email or Password. Try again!')
            }



         },
         dataType : 'json'
         });
     // ajax call

    });

     //VentForm
     $('#ventBtn').click(function(){
        
        var vent_message =$('#ventMessage').val();
        if(vent_message == "" || vent_message == null){
            toastr.error('Enter Vent Message then try Submit');
            return false;
        }else{
            var formData = {
                'vent_message':vent_message,
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: BASE_URL+'/vent/addVent',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        toastr.success(res.txt);
                         return false;
                    }else{
                        toastr.error(res.txt);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });

//btnSellerEnter
// $('.sellerApply').hide();
$('#btnSellerEnter').click(function(){
    var txtSellerCode=$('#txtSellerCode').val();
    if(txtSellerCode==""){
        toastr.error('Invalid code');
        return false;
    }
    //ajax
    var formData = {
        'txtSellerCode':txtSellerCode, 
        'csrf_token':$('meta[name="csrf_token"]').attr('content')
     };
     $.ajax({
        url: BASE_URL+'/index/seller_code',
        type: 'POST',
        data: formData,
        success: function(res) {
            if(res.status==1){
                toastr.success(res.txt);
                $('.sellerApply').show();
                 return false;
            }else{
                toastr.error(res.txt);
                return false;
            }       
        },
         dataType : 'json'
    });
    //ajax
});
//btnSellerEnter


    //btnLogin
    //add to cart
    $('.cart_btn').click(function(){
        product_id=$(this).attr('id');
        
      var formData = {
        'product_id':product_id,       
        'quantity':1,       
        'csrf_token':$('meta[name="csrf_token"]').attr('content')
     };

     $.ajax({
        url: BASE_URL+'/product/addToCart',
        type: 'POST',
        data: formData,
        success: function(res) {
        
         if(res.status==1){             
            toastr.success(res.txt);
            var delay = 1000; 
            var url = BASE_URL+"/product/carts";    
            setTimeout(function(){ window.location = url; }, delay);

          }
        },
        dataType : 'json'
        });
    });
    //add to cart

    //remove from cart
    $('.removeItem').click(function(){
        code=$(this).attr('id');
        
      var formData = {
        'code':code,       
        
        'csrf_token':$('meta[name="csrf_token"]').attr('content')
     };

     $.ajax({
        url: BASE_URL+'/product/removeFromCart',
        type: 'POST',
        data: formData,
        success: function(res) {
         console.log(res);
         if(res.status==1){             
            toastr.success(res.txt);

            setTimeout(function() {
                location.reload();
            }, 1000);

          }

        },
        dataType : 'json'
        });



    });
    //remove from cart


});
/*Registration Process*/
jQuery(function ($) {
    $("#serviceOption").click(function(){
        if($( "#planMemberbasic" ).hasClass( "active" ).toString() == 'true' || 
        $( "#planMemberstandard" ).hasClass( "active" ).toString() == 'true' ||
        $( "#planMemberpremium" ).hasClass( "active" ).toString() == 'true' ){            
            var SelectedPlan = $("#planSelected .active .planname").attr('id');
            localStorage.setItem('SelectedPlan', SelectedPlan);
            toastr.success("You have Selected : " +  SelectedPlan + " Plan");
            $(window).attr("location",BASE_URL+"/registrationstep3");
        }else{
            console.log($( "#planMemberbasic" ).hasClass( "active" ).toString());
            toastr.error("Select any of the plan to continue");
        }        
    });
    $("#cerateAccount").click(function(){
        var user_name = $('#name').val();
        var user_email = $('#email').val();
        var pass = $('#pass').val();
        var store_name = $('store_name').val();
        var plan_selected = localStorage.getItem('SelectedPlan');
        if(user_name == "" || user_email == "" || pass == "" || store_name == ""){
            toastr.error('All fields are required!');
            return false;
        }else{
            var formData = {
                'user_name':user_name,
                'user_email':user_email,
                'pass':pass,
                'store_name':store_name,
                'plan_selected':plan_selected,
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: BASE_URL+'/createAccount/addUser',
                type: 'POST',
                data: formData,
                success: function(res) {
                
                    if(res.status==1){
                        toastr.success(res.txt);
                         return false;
                    }else{
                        toastr.error(res.txt);
                        return false;
                    }

                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });
    // <?=Config::get('URL'); ?>registrationstep3
    /*Apply Affiliate*/    
    $("#affiliateApply").click(function(){
        var user_name = $('#name').val();
        var user_email = $('#email').val();
        var socialName = $('#socialName').val();
        var noFollower = $('#noFollower').val();
        var onYoutube = $("input[name='followers']:checked").val();
        var noofYoutube = $('#youtubeSubscriberNo').val();
        var message = $('#message').val();
        if(user_name == "" || user_email == "" || socialName == "" || message == ""){
            toastr.error('All fields are required!');
            return false;
        }else{
        var formData = {
            'user_name':user_name,
            'user_email':user_email,
            'socialName':socialName,
            'noFollower':noFollower,
            'onYoutube':onYoutube,
            'noofYoutube':noofYoutube,
            'message': message,
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/applyaffiliate/addAffiliate',
            type: 'POST',
            data: formData,
            success: function(res) {
            
                if(res.status==1){
                    toastr.success(res.txt);
                    $(".loader").hide();
                    return false;
                }else{
                    toastr.error(res.txt);
                    $(".loader").hide();
                    return false;
                }

            
            },
            dataType : 'json'
        });
        // ajax call
        }
    });
    // <?=Config::get('URL'); ?>registrationstep3

     /*Apply Seller*/    
     $("#sellerApply").click(function(){
        var business_name = $('#name').val();
        var user_email = $('#email').val();
        var website = $('#website').val();
        var fromUS = $("input[name='location']:checked").val();
        var city = $("#city").val();
        var ship = $("input[name='ship']:checked").val();
        var message = $('#message').val();

        if(business_name == "" || user_email == "" || website == "" || message == ""){
            toastr.error('All fields are required!');
            return false;
        }else{
        var formData = {
            'business_name':business_name,
            'user_email':user_email,
            'website':website,
            'fromUS':fromUS,
            'city':city,
            'ship':ship,
            'message': message,
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/sellerapply/addseller',
            type: 'POST',
            data: formData,
            success: function(res) {
            
                if(res.status==1){
                    toastr.success(res.txt);
                    $(".loader").hide();
                    return false;
                }else{
                    toastr.error(res.txt);
                    $(".loader").hide();
                    return false;
                }

            
            },
            dataType : 'json'
        });
        // ajax call
        }
    });
    // <?=Config::get('URL'); ?>registrationstep3
    $('#subscribe').click(function(){
        var email=$('#subs_email').val();
        if(email==""){
            toastr.error('Enter email Id');
            return false;
        }
        //ajax
        var formData = {
            'email':email, 
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
         };
         $.ajax({
            url: BASE_URL+'/sellerapply/subscribe',
            type: 'POST',
            data: formData,
            success: function(res) {
                if(res.status==1){
                    toastr.success(res.txt);                   
                    $(".stall").hide(1000);
                    return false;
                }else{
                    toastr.error(res.txt);
                    $(".stall").hide(1000);
                    return false;
                }       
            },
             dataType : 'json'
        });      
    
    });

});