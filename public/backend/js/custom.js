// nav fixed 
$("document").ready(function () {
    $(window).scroll(function () {
        if ($(window).scrollTop() > 200) {
            $('.menu').addClass('fixed-top');
        } else {
            $('.menu').removeClass('fixed-top');
        }
    });

    $('.top-bar .image img').click(function () {
        $('.top-bar .image ul').slideToggle();
    });

    $('.admin-orders .content').click(function () {
        productId= $(this).attr("product_id");
        brand_name =$(this).find("#brandName").attr("brandName");
        productName =$(this).find("#productName").attr("productName");
        sellerPrice =$(this).find("#sellerPrice").attr("sellerPrice");
        $("#productId").val(productId);
        $("#brand_name").val(brand_name);
        $("#product_name").val(productName);
        $("#old_price").val(sellerPrice);
        $('.search_content').slideDown();
        $('.soverlay').slideDown();     

    });
    $('#updateProduct').click(function () {
        var productId = $("#productId").val();
        var brand_name = $("#productId").val();
        var productName = $("#product_name").val();
        var sellerPrice = $("#sellerPrice").val();
        var newPrice = $("#new_price").val();
        var formData = {
            'productId':productId,
            'brand_name':brand_name,
            'productName': productName,
            'sellerPrice':sellerPrice,
            'newPrice':newPrice,
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/admin/updateproduct',
            type: 'POST',
            data: formData,
            success: function(res) {
                console.log(res);
                $('#txtUNCode').val(res);
                toastr.success('Code Generated');
                return false;
            
            },
        
        });
    });

    $('.soverlay').click(function () {
        $('.search_content,.soverlay').slideUp();
    });



    $('.carousel101 .owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    
    
    $('.admin-messenger .bar .owl-carousel').owlCarousel({
        loop: true,
        margin: 20,
        nav: true,
        navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });
});



$('document').ready(function () {
    //high chart
    $('.demo1').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#ff6c40', '#d8d8d8']
    });


    //high chart
    $('.demo2').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#FF5252', '#777777']
    });



    //high chart
    $('.demo3').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#FF5252', '#777777']
    });


});

// seller analytics
$('document').ready(function () {
    //high chart
    $('.demo4').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#ff00b8', '#d8d8d8']
    });

    //high chart
    $('.demo5').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#ff00b8', '#d8d8d8']
    });


    //high chart
    $('.demo6').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#ff00b8', '#d8d8d8']
    });


    //high chart
    $('.demo7').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#ff00b8', '#d8d8d8']
    });
});




//all popups

$('document').ready(function () {
    $('.affaliate_grid .content img').click(function () {
        $('.first-sale, .overlay').fadeIn();
    });
    $('.overlay').click(function () {
        $('.first-sale, .overlay').fadeOut();
    });



    $('.search_bar .buttons .delete').click(function () {
        $('.del10').show();
        $('.admin-orders .content').hide();
    });
});




$('document').ready(function () {
    $('.del10 .box span').click(function () {
        $('.confirm_delet').fadeIn();
    });
    $('.confirm_delet .overlay, .confirm_delet button').click(function () {
        $('.confirm_delet').fadeOut();
    });
    $('.live').click(function () {
        $('.live_popup').fadeIn();
    });
    $('.live_popup button, .live_button .overlay').click(function () {
        $('.live_popup').fadeOut();
    });
    $('.mobile-bar .profile img').click(function () {
        $('.mobile-bar .profile ul').slideToggle();
    });


    $('.mobile-bar .menu_icon img').click(function () {
        $('.menu').addClass('active');
        $('.moverlay').addClass('activee');
    });
    $('.moverlay').click(function () {
        $('.menu').removeClass('active');
        $('.moverlay').removeClass('activee');
    });
});
// admin dashbord 

$('document').ready(function () {
    //high chart
    $('.demo8').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
        lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    //high chart
    $('.demo9').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    //high chart
    $('.demo10').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    //high chart
    $('.demo11').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    //high chart
    $('.demo12').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    //high chart
    $('.demo13').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


    //high chart
    $('.demo14').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


   //high chart
    $('.demo15').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });

    //high chart
    $('.demo16').blueberryChart({
        chartData: [
                [40, 90, 43, 78, 45],
                [32, 67, 23, 20, 41]
            ],
        chartLables: ['A', 'B', 'C', 'D', 'E'],
        showLables: true,
        showLines: true,
        showDots: false,
         lineColors: ['#000', '#d8d8d8']
    });


});




$('.sellerApply').hide();
//admin key
$('document').ready(function () {
    $('.affaliate_grid .content').click(function () {
        $('.admin_code').fadeIn();
    });
    $('.admin_code .cencel').click(function () {
        $('.admin_code').fadeOut();
    });

    //admin area
    $('#btnSellerGenCode').click(function(){
        $('#kt_modal_5').modal('show');
    });

    $('#btnGenerateSellercode').click(function(){
        var formData = {
            'seller_code':'seller_code',
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/admin/seller_code',
            type: 'POST',
            data: formData,
            success: function(res) {
            console.log(res);
            $('#txtUNCode').val(res);
            toastr.success('Code Generated');
            return false;         
               

            
           },
           
        });

    });
    //AffiliateApproved
    $('.affiliateApproved').click(function(){
        
        var formData = {
            'affiliate_id':$(this).attr('id'),
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/admin/affiliate_approved',
            type: 'POST',
            data: formData,
            success: function(res) {
                if(res.status==1){
                   
                    toastr.success(res.txt);
                     return false;
                }else{
                    toastr.error(res.txt);
                    return false;
                }       
               

            
           },
           dataType : 'json'
           
            });

    });

    //AffiliateApproved

    
    //sellerApproved/
    $('.sellerApproved').click(function(){
        
        var formData = {
            'seller_id':$(this).attr('id'),
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/admin/seller_approved',
            type: 'POST',
            data: formData,
            success: function(res) {
                if(res.status==1){
                   
                    toastr.success(res.txt);
                     return false;
                }else{
                    toastr.error(res.txt);
                    return false;
                }       
               

            
           },
           dataType : 'json'
           
            });

    });
    //sellerApproved

    $('#btnSaveAuthCode').click(function(){
        var formData = {
            'seller_code':$('#txtUNCode').val(),
            'csrf_token':$('meta[name="csrf_token"]').attr('content')
        };

        $.ajax({
            url: BASE_URL+'/admin/seller_code_save',
            type: 'POST',
            data: formData,
            success: function(res) {
                if(res.status==1){
                    $('.sellerApply').show();
                    toastr.success(res.txt);
                     return false;
                }else{
                    toastr.error(res.txt);
                    return false;
                }       
               

            
           },
           dataType : 'json'   
        });

    });

    

    //admin area

    $("#drop-container").on('dragenter', function(e) {
        e.preventDefault();
        $(this).css('border', '#39b311 2px dashed');
        $(this).css('background', '#f1ffef');
    });

    $("#drop-container").on('dragover', function(e) {
        e.preventDefault();
    });

    $("#drop-container").on('drop', function(e) {
        $(this).css('border', '#07c6f1 2px dashed');
        $(this).css('background', '#FFF');
        e.preventDefault();
        var image = e.originalEvent.dataTransfer.files;
        createFormData(image);
    });
    function createFormData(image) {
        var formImage = new FormData();
        formImage.append('dropImage', image[0]);
        uploadFormData(formImage);
    }

    function uploadFormData(formData) {
        $.ajax({
        url: aBASE_URL+'/seller/updateProductIMG',
        type: "POST",
        data: formData,
        contentType:false,
        cache: false,
        processData: false,
        success: function(response){
            var imagePreview = $(".drop-image").clone();
            imagePreview.attr("src", response); 
            imagePreview.removeClass("drop-image");
            imagePreview.addClass("preview");
            $('#drop-container').append(imagePreview);
        }
    });
    }

    //--------------------

    //Profile update Ajax
    $(document).on('click', '#update_profile', function(){
        //updateprofile
        $("#saveSettings").attr("disabled", true);
        var firtstname = $("#first_name").val();
        var lastname = $("#last_name").val();
        var phone = $("#phone").val();
        var email = $("#email").val();
        var current_password = $("#current_password").val();
        var new_password = $("#new_password").val();
        var re_password = $("#re_password").val();
        //add update code here 
        if(new_password != "" && re_password != "" && current_password != ""){
            if($.trim(re_password) !== $.trim(new_password)){
                console.log(re_password + new_password);
                toastr.error('New password must be equal to re-password');
                return false;
            }
        }
        if((new_password != "" && (current_password == "" || re_password =="")) ||
        (current_password != "" && (new_password == "" || re_password =="")) ||
        (re_password !="" && (new_password == "" || current_password == "")) ){
            toastr.error('Password change require all password field to be entered');
            return false;
        }
        if(firtstname == "" || lastname =="" ||phone =="" || email =="" ){
            toastr.error('Empty field not allowed');
        }else{
            var formData = {
                'firtstname': firtstname,
                'lastname': lastname,
                'phone': phone,
                'email': email,
                'current_password':current_password,
                'new_password':new_password,
                're_password':re_password,
                'csrf_token': $('meta[name="csrf_token"]').attr('content')
            };
            console.log(formData);
            $.ajax({
                type: "POST",
                url: BASE_URL+'/user/updateProfile',
                data: formData,
                success: function(res){
                    if(res.status==1){
                        // toastr.success(res.txt);
                        toastr.success("Setting has saved successfully!")
                        return false;
                    }else if(res.status == 2){
                         // toastr.success(res.txt);
                         toastr.error("Current Password in Invalid, Try again with Valid data!")
                         return false;
                    }else if(res.status == 3){
                        // toastr.success(res.txt);
                        toastr.success("User status updated")
                        return false;
                   }
                    else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#saveSettings").attr("disabled", false);
                        return false;
                    }
                },
                dataType : 'json'

            });
        }	
    });


});











