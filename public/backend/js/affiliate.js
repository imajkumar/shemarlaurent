$('document').ready(function () {
    if(window.location.pathname == "affiliate/messengers"){
        displayChat();
    } 

    function displayChat(){
        var agentid = $("#chatagent").val();
        $.ajax({
            url: 'fetchchat',
            type: 'POST',
            async: false,
            data:{
                id: agentid  
            },
            success: function(response){
                $('#chat_area').html(response);
                $("#chat_area").scrollTop($("#chat_area")[0].scrollHeight);
            }
        });
    }

    $(document).on('click', '#send_msg', function(){
        var agentid = $("#chatagent").val();
        if($('#chat_msg').val() == ""){
            toastr.error('Please write message first');
        }else{
            $msg = $('#chat_msg').val();
            $.ajax({
                type: "POST",
                url: "sendChat",
                data: {
                    msg: $msg,
                    senderid: agentid,
                },
                success: function(){
                    $('#chat_msg').val("");
                    displayChat();
                }
            });
        }	
    });

    //Update Profile
    $(document).on('click', '#enableEdit', function(){
        $('#email').prop("disabled", false); 
    })
        
   
    //Validate Email Sent
    $(document).on('click', '#changeEmail', function(){
        $("#updateEm").toggle();        
    });
    $(document).on('click', '#verifyEmail', function(){
        var userEmail = $(this).attr('email');
        toastr.error('here we send mail for verification'+ userEmail);
        //ajax call for send verification
       
       
        // //add update code here 
        // if(userEmail == ""){
        //     toastr.error('Invalid data request');
        // }else{
        //    
        //     $.ajax({
        //         type: "POST",
        //         url: "sendChat",
        //         data: {
        //             msg: userEmail
        //         },
        //         success: function(){
        //           toastr.success('Email Updated Successfully');   
        //         }
        //     });
        // }	
    });
    $(document).on('click', '#update', function(){
        var userEmail = $("#userEmail").val();
        toastr.error('Update Email'+ userEmail);     
       
        //add update code here 
        if(userEmail == ""){
            toastr.error('Invalid data request');
        }else{
           
            $.ajax({
                type: "POST",
                url: "emailupdate",
                data: {
                    msg: userEmail
                },
                success: function(){
                  toastr.success('Email Updated Successfully');   
                }
            });
        }	
    });

    $(document).on('click', '#saveSetting', function(){
        $("#saveSetting").attr("disabled", true);
        var store_name = $('#store_name').val();
        var email = $('#email').val();   
        if(store_name == "" || email ==""){
            toastr.error('All fields are required');
            return false;
        }else{
            var formData = {
                'store_name':store_name,              
                'email': email,               
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: 'affsetting',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        // toastr.success(res.txt);
                        $("#saveSetting").attr("disabled", false);
                        toastr.success("Setting has saved successfully!")
                        return false;
                    }else if(res.status == 3){
                        // toastr.error(res.txt);
                        toastr.info("Setting Updated!")
                        $("#saveSetting").attr("disabled", false);
                        return false;
                    }else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#saveSetting").attr("disabled", false);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });
    $(document).on('click', '#paymentSetting', function(){
        $("#paymentSetting").attr("disabled", true);
        var account_no = $('#account_no').val();
        var routing_no = $('#routing_no').val();       

        if(account_no == "" || routing_no ==""){
            toastr.error('All fields are required');
            return false;
        }else{
            var formData = {
                'account_no':account_no,              
                'routing_no': routing_no,               
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: 'paymentSetting',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        // toastr.success(res.txt);
                        toastr.success("Setting has saved successfully!")
                        return false;
                    }else if(res.status == 3){
                        // toastr.error(res.txt);
                        toastr.info("Payment setting Updated!")
                        $("#paymentSetting").attr("disabled", false);
                        return false;
                    }else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#paymentSetting").attr("disabled", false);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });

});