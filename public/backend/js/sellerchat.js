$('document').ready(function () {
    if(window.location.pathname == "/corephp/sellermessanger"){
        displayChat();
    }

    function displayChat(){
        var agentid = $("#chatagent").val();
        $.ajax({
            url: 'sellermessanger/fetchchat',
            type: 'POST',
            async: false,
            data:{
                id: agentid              
            },
            success: function(response){
                $('#chat_area').html(response);
                $("#chat_area").scrollTop($("#chat_area")[0].scrollHeight);
            }
        });
    }

    $(document).on('click', '#send_msg', function(){
        //chatroomid
        var agentid = $("#chatagent").val();
        if($('#chat_msg').val() == ""){
            toastr.error('Please write message first');
        }else{
            $msg = $('#chat_msg').val();
            $.ajax({
                type: "POST",
                url: "sellermessanger/sendChat",
                data: {
                    msg: $msg,
                    senderid: agentid,
                },
                success: function(){
                    $('#chat_msg').val("");
                    displayChat();
                }
            });
        }	
    });

    //Update Profile
    $(document).on('click', '#enableEdit', function(){
        $('#email').prop("disabled", false); 
    })        
    $(document).on('click', '#saveSettings', function(){
        $("#saveSettings").attr("disabled", true);
        var store_name = $('#store_name').val();
        var store_email = $('#store_email').val();
        var legal_name = $('#legal_name').val();
        var phone = $('#phone').val();
        var address = $('#address').val();
        var apartment = $('#apartment').val();
        var city = $('#city').val();
        var country = $('#country').val();
        var state = $('#state').val();
        var zip = $('#zip').val();
        var phone_no = $('#phone_no').val();

        if(store_name == "" || store_email == "" || legal_name == "" || phone == "" || address =="" || apartment == "" ||
        city == "" || country == "" || state == "" || zip == "" || phone_no == ""){
            toastr.error('All fields are required');
            return false;
        }else{
            var formData = {
                'store_name':store_name,
                'store_email': store_email,
                'legal_name': legal_name,
                'phone':phone,
                'address': address,
                'apartment': apartment,
                'city':city,
                'country':country,
                'state':state,
                'zip':zip,
                'phone_no':phone_no,
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: 'sellersetting',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        // toastr.success(res.txt);
                        $("#saveSettings").attr("disabled", false);
                        toastr.success("Setting has saved successfully!")
                         return false;
                    }if(res.status == 3){
                         // toastr.success(res.txt);
                         $("#saveSettings").attr("disabled", false);
                         toastr.success("Setting has Updated successfully!")
                         return false;
                    }else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#saveSettings").attr("disabled", false);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });
    $(document).on('click', '#shipSettings', function(){
        $("#shipSettings").attr("disabled", true);
        var location = $('#location').val();
        var address = $('#address').val();
        var apartment = $('#apartment').val();
        var city = $('#city').val();
        var country = $('#country').val();
        var state = $('#state').val();
        var zip = $('#zip').val();
        var phone_no = $('#phone_no').val();

        if(location == "" || address =="" || apartment == "" ||
        city == "" || country == "" || state == "" || zip == "" || phone_no == ""){
            toastr.error('All fields are required');
            return false;
        }else{
            var formData = {
                'location':location,              
                'address': address,
                'apartment': apartment,
                'city':city,
                'country':country,
                'state':state,
                'zip':zip,
                'phone_no':phone_no,
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: 'shipsetting',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        // toastr.success(res.txt);
                        toastr.success("Setting has saved successfully!")
                         return false;
                    }else if(res.status ==3){
                        // toastr.error(res.txt);
                        toastr.info("Settings has been Updated!")
                        $("#shipSettings").attr("disabled", false);
                        return false;
                    }else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#shipSettings").attr("disabled", false);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });
    $(document).on('click', '#paymentSetting', function(){
        $("#paymentSetting").attr("disabled", true);
        var account_no = $('#account_no').val();
        var routing_no = $('#routing_no').val();       

        if(account_no == "" || routing_no ==""){
            toastr.error('All fields are required');
            return false;
        }else{
            var formData = {
                'account_no':account_no,              
                'routing_no': routing_no,               
                'csrf_token':$('meta[name="csrf_token"]').attr('content')
            };

            $.ajax({
                url: 'paymentSetting',
                type: 'POST',
                data: formData,
                success: function(res) {                
                    if(res.status==1){
                        // toastr.success(res.txt);
                        toastr.success("Setting has saved successfully!")
                        return false;
                    }else if(res.status == 3){
                        // toastr.error(res.txt);
                        toastr.info("Payment Setting Updated!")
                        $("#paymentSetting").attr("disabled", false);
                        return false;
                    }else{
                        // toastr.error(res.txt);
                        toastr.error("There is some issue!")
                        $("#paymentSetting").attr("disabled", false);
                        return false;
                    }
                
               },
                dataType : 'json'
                });
            // ajax call
        }
    });   

});