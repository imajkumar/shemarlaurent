﻿﻿function Page(){

   
    var self= this;
    var timeout = 0;
    var status = 0;
    var running = 0;
    var el;
    var w = $(window);
    var clock = $('.countDown span');
    var SPINTAX_PATTERN = /\{[^"\r\n\}]*\}/;
    var ItemPost = [];
    this.init= function(){       
        self.CorePost(); 
        self.showNotification(); 
        
        
       

    };
    this.showNotification = function(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
        if (colorName === null || colorName === '') { colorName = 'bg-black'; }
        if (text === null || text === '') { text = 'Welcome'; }
        if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
        if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
        var allowDismiss = true;
      
      if (typeof text !== 'undefined'){
        $.notify({
            message: text
        },
            {
                type: colorName,
                allow_dismiss: allowDismiss,
                newest_on_top: true,
                timer: 1000,
                placement: {
                    from: placementFrom,
                    align: placementAlign
                },
                animate: {
                    enter: animateEnter,
                    exit: animateExit
                },
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>' 
            });
      }
        
    };


   

    this.CorePost = function(){


         // ajcode
         $("#frmAddProduct").on('submit',(function(e) {
            e.preventDefault();
            // $("#message").empty();
            // $('#loading').show();
            $.ajax({
            url: BASE_URL+'/seller/SaveProducts', // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(result)   // A function to be called if request succeeds
            {
                self.showNotification(result['label'], result['txt'], 'top', 'right', 'animated bounceIn', 'animated bounceOut');
    
                setTimeout(function(){

                 location.reload();
                    
               },3000);
                   
            },
            dataType : 'json'    
            });
            }));

        // ajcode

        $(document).on('click', '.btnAct5ionPost', function(){

            _that    = $(this);
            _form     = _that.closest("form");
             token     = $('meta[name="csrf_token"]').attr('content');
            _action   = _form.attr("action");
            _redirect = _form.data("redirect");
            _data     = _form.serialize();
            _data     = _data + '&' + $.param({csrf_token:token});
           
           // $(".page-loader-action").fadeIn();
            if(!_form.hasClass('disable')){
               // _form.addClass('disable');                
                $.post(_action, _data, function(result){
                    _form.removeClass('disable');
                    $(".page-loader-action").fadeOut();
                    if(result['st'] == "success")
                    setTimeout(function(){
                        window.location.assign(_redirect);
                    },3000);
                    self.showNotification(result['label'], result['txt'], 'top', 'right', 'animated bounceIn', 'animated bounceOut');
                                           
                },'json');
            }
            return false;
        });


        
    }

    
}
Page= new Page();
$(function(){
    Page.init();
});

