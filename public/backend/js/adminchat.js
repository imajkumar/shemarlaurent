$('document').ready(function () {
    
    if(window.location.pathname == "/corephp/admin/messanger"){
       
        setTimeout(function() { 
            displayChat();
            fetchUsers();
         }, 1000);
    } 

   

    function displayChat(){
        var agentid = $("#chatagent").val();        
        $.ajax({
            url: 'fetchchat',
            type: 'POST',
            async: false,
            data:{
                id: agentid
            },
            success: function(response){
                $('#chat_area').html(response);
                $("#chat_area").scrollTop($("#chat_area")[0].scrollHeight);
            }
        });
    }
    
    function fetchUsers(){
        $.ajax({
            url: 'chatUser',
            type: 'POST',
            async: false,
            data:{
            },
            success: function(response){
                $('#chatUsers').html(response);
            }
        });
    }

    $(document).on('click', '#send_msg', function(){
   
        var agentid = $("#chatagent").val();
        var msg =  $("#chat_msg").val();
        if( msg == "" ){
            toastr.error('Please write message first');
        }else{            
            $.ajax({
                type: "POST",
                url: "sendchat",
                data: {
                    msg: msg,
                    senderid: agentid,
                },
                success: function(){                    
                    $('#chat_msg').val("");
                    displayChat();
                }
            });
        }	
    });
 });